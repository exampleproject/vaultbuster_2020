﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CellButton : MonoBehaviour, IPointerEnterHandler, IPointerDownHandler
{
    public UnityEventCellButton Selected;
    public UnityEventCellButton Deselected;
    public bool IsSelectable { get; set; }

    public bool IsOn
    {
        set
        {
            if (IsSelectable)
            {
                GetComponentInChildren<Image>(true).gameObject.SetActive(value);
                _isOn = value;
                if (_isOn)
                {
                    Selected.Invoke(this);
                }
                else
                {
                    Deselected.Invoke(this);
                }
            }
            else
            {
                GetComponentInChildren<Image>(true).gameObject.SetActive(false);
                _isOn = false;
            }
        }
        get
        {
            return _isOn;
        }
    }
    private bool _isOn;

    public int Value
    {
        set
        {
            _value = value;
            GetComponentInChildren<Text>().text = _value.ToString("D2");
        }
        get
        {
            return _value;
        }
    }
    private int _value;

    public void OnPointerEnter(PointerEventData eventData)
    {
#if UNITY_EDITOR
        if (Input.GetMouseButton(0))
        {
            IsOn = !IsOn;
        }
#elif UNITY_ANDROID
        if (Input.touchCount > 0)
        {
            IsOn = !IsOn;
        }
#endif
    }

    public void OnPointerDown(PointerEventData eventData)
    {
#if UNITY_EDITOR
        IsOn = !IsOn;
#endif
    }
}