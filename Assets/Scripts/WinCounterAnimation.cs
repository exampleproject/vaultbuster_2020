﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class WinCounterAnimation : MonoBehaviour
{
    public float AnimationTime = 1;
    public Vector2 StartPoint = new Vector2(0, -950);
    public AnimationCurve AccelerationCurve;
    public AnimationCurve DecelerationCurve;

    private Vector2 _endPoint;

    private void OnEnable()
    {
        StartCoroutine(Animation());
    }

    IEnumerator Animation()
    {
        var sprites = GetComponentsInChildren<RectTransform>(true).Where(rt => rt.gameObject != gameObject).ToList();
        _endPoint = sprites[0].anchoredPosition;

        var timeBetweenSprites = AnimationTime / (transform.childCount * 2);
        var pathTime = AnimationTime * 0.5f;

        for (int i = 0; i < sprites.Count; i++)
        {
            StartCoroutine(Path(pathTime, sprites[i]));
            yield return new WaitForSeconds(timeBetweenSprites);
        }
        yield return new WaitForSeconds(AnimationTime * 0.5f);
        gameObject.SetActive(false);
    }

    IEnumerator Path(float time, RectTransform sprite)
    {
        sprite.gameObject.SetActive(true);

        var halfTime = time * 0.5f;
        var halfPoint = (StartPoint + _endPoint) * 0.5f;

        for (float timer = 0; timer < halfTime; timer += Time.deltaTime)
        {
            var curvePercent = AccelerationCurve.Evaluate(timer / halfTime);
            sprite.anchoredPosition = Vector2.Lerp(StartPoint, halfPoint, curvePercent);
            yield return null;
        }
        sprite.anchoredPosition = halfPoint;
        for (float timer = 0; timer < halfTime; timer += Time.deltaTime)
        {
            var curvePercent = DecelerationCurve.Evaluate(timer / halfTime);
            sprite.anchoredPosition = Vector2.Lerp(halfPoint, _endPoint, curvePercent);
            yield return null;
        }
        sprite.anchoredPosition = _endPoint;
        sprite.gameObject.SetActive(false);
    }
}
