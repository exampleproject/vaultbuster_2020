﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Globalization;

public class DailyTimer : MonoBehaviour
{
    public Button DailyButton;
    public Button DailyTimerButton;
    public TextMeshProUGUI TimerText;

    private const string TIMER_FORMAT = "{0}H : {1}M : {2}S";

    private void OnEnable()
    {
        StartCoroutine(Timer());
    }

    IEnumerator Timer()
    {
        if (AppManager.App != null && AppManager.App.CurrentUser != null)
        {
            if (!string.IsNullOrEmpty(AppManager.App.CurrentUser.NextDaily))
            {
                DateTime nextDaily;
                if (DateTime.TryParseExact(AppManager.App.CurrentUser.NextDaily, UIReferences.DATE_FORMAT, CultureInfo.InvariantCulture, DateTimeStyles.AllowInnerWhite, out nextDaily))
                {
                    var timeGap = nextDaily - DateTime.Now;
                    Debug.Log(nextDaily);
                    Debug.Log(DateTime.Now);
                    Debug.Log(timeGap.TotalSeconds);
                    while (timeGap.TotalSeconds > 1)
                    {
                        timeGap = nextDaily - DateTime.Now;
                        TimerText.text = string.Format(TIMER_FORMAT, timeGap.Hours, timeGap.Minutes, timeGap.Seconds);

                        DailyButton.gameObject.SetActive(false);
                        DailyTimerButton.gameObject.SetActive(true);
                        yield return new WaitForSeconds(1);
                    }
                }
            }
            DailyButton.gameObject.SetActive(true);
            DailyTimerButton.gameObject.SetActive(false);
        }
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }
}
