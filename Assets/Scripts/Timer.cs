﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public int Seconds;
    public TMPro.TextMeshProUGUI TimerText;
    public Image TimerBar;
    public UnityEvent TimeIsUp;
    public int CurrentTime { get; private set; }
    public bool IsPause { get; private set; }
    private Coroutine _countTime;

    public void StartTimer(int seconds = -1)
    {
        IsPause = false;
        if (seconds == -1)
        {
            CurrentTime = Seconds;
        }
        else
        {
            CurrentTime = seconds;
        }
        _countTime = AppManager.App.GetComponent<MonoBehaviour>().StartCoroutine(CountTime());
    }

    public void PauseTimer()
    {
        IsPause = true;
    }

    public void ContinueTimer()
    {
        IsPause = false;
    }

    IEnumerator CountTime()
    {
        while (CurrentTime > 0)
        {
            if (!IsPause)
            {
                TimerText.text = CurrentTime.ToString();
                TimerBar.fillAmount = (float)CurrentTime / Seconds;
                CurrentTime--;
                yield return new WaitForSecondsRealtime(1);
            }
            else
            {
                yield return null;
            }
        }
        TimerText.text = 0.ToString();
        TimerBar.fillAmount = 0;
        TimeIsUp.Invoke();
        StopTimer();
    }

    public void AddTime(int timeToAdd)
    {
        CurrentTime = Mathf.Min(Seconds, CurrentTime + timeToAdd);
    }

    public void SubtractTime(int timeToSubtract)
    {
        CurrentTime = Mathf.Max(0, CurrentTime - timeToSubtract);
    }

    public void StopTimer()
    {
        if (_countTime != null)
        {
            AppManager.App.GetComponent<MonoBehaviour>().StopCoroutine(_countTime);
        }
        CurrentTime = 0;
    }

    public void ResetTimer()
    {
        StopTimer();
        TimerBar.fillAmount = 1;
    }
}
