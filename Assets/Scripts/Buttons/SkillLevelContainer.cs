﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillLevelContainer : MonoBehaviour
{
    private void OnEnable()
    {
        var buttons = transform.GetComponentsInChildren<SkillLevelButton>();
        var min = 0;
        var upTo = GameProgressConfig.GameProgress.GetUpTo();
        for (var i = 0; i < buttons.Length - 1; i++)
        {
            buttons[i].Min = min;
            buttons[i].Max = upTo[i];
            buttons[i].SetButton();
            min = upTo[i];
        }
    }
}
