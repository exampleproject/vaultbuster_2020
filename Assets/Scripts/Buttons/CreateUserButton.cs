﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateUserButton : MonoBehaviour
{
    public TMPro.TMP_InputField NameField;

    private Button _createButton;

    private void Start()
    {
        _createButton = GetComponent<Button>();
    }

    private void Update()
    {
        if (NameField.text.Length >= 1)
        {
            _createButton.interactable = true;
        }
        else
        {
            _createButton.interactable = false;
        }
    }
}
