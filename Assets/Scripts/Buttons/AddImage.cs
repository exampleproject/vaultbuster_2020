﻿using System.IO;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using UnityEngine.Android;
//using GracesGames.SimpleFileBrowser.Scripts;

public class AddImage : MonoBehaviour
{
    public Image Background;
    public Toggle DefaultToggle;
    public GameObject AndroidBrowserPrefab;

    private Sprite _addImageSprite;
    private PlayerImage _playerImage;
    //private FileBrowser _androidBrowser;

    private void Start()
    {
        _addImageSprite = Background.sprite;
        _playerImage = GetComponentInParent<PlayerImage>();
    }

    public void OnAddImage(bool isOn)
    {
//        if (isOn)
//        {
//#if UNITY_EDITOR
//            var imageFile = EditorUtility.OpenFilePanel("Choose Image", string.Empty, "jpg,png");
//            SetImageFile(imageFile);
//#elif UNITY_ANDROID
//            //NativeGallery.GetImageFromGallery(i => SetImageFile(i));
//            var androidBrowserGO = Instantiate(AndroidBrowserPrefab, transform);
//            _androidBrowser = androidBrowserGO.GetComponent<FileBrowser>();
//            _androidBrowser.OpenFilePanel(new string[] { "png", "jpg" });
//		    //_androidBrowser.OnFileSelect += SetImageFile;
//            //androidBrowser.OnFileBrowserClose += OnfileBrowserClose;
//            //_androidBrowser.FileBrowserCloseEvent.AddListener(OnfileBrowserClose);
//#endif
//        }
//        else
//        {
//            Background.sprite = _addImageSprite;
//        }

    }

    public void SetImageFile(string imageFile)
    {
        //UIReferences.References.DebugText.text = "Return";
        if (!string.IsNullOrEmpty(imageFile) && File.Exists(imageFile))
        {
            _playerImage.ImageUrl = imageFile;

            byte[] fileData;
            fileData = File.ReadAllBytes(imageFile);
            var tex = new Texture2D(2, 2);
            if (tex.LoadImage(fileData))
            {
                Background.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector2.zero);
            }
        }
        else
        {
            if (Background.sprite == _addImageSprite)
            {
                DefaultToggle.isOn = true;
            }
        }
        //_androidBrowser.Destroy();
    }
}
