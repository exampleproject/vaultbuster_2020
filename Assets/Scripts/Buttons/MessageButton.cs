using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessageButton : MonoBehaviour
{
    public ShopManager.Tabs ShopTab;
    public int MinGold = 25;
    public int MaxGoldBarReminderTimes = 3;

    public void OnBuyNowClick()
    {
        UIReferences.References.ActiveScreen(ScreensManager.Screens.SHOP, ScreensManager.Requires.SHOP);
        if (AppManager.App.CurrentUser.Vault.GoldBars < MinGold && GameConfigManager.GameConfig.LowGoldBarsNotificationCounter < MaxGoldBarReminderTimes)
        {
            ShopManager.Shop.SetTab(ShopManager.Tabs.Gold);
            UIReferences.References.ShowMessage(UIReferences.MessagesTypes.ReminderMessage);
            GameConfigManager.GameConfig.LowGoldBarsNotificationCounter++;
        }
        else
        {
            ShopManager.Shop.SetTab(ShopTab);
        }
    }
}
