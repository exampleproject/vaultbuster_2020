﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadFirebase : MonoBehaviour
{
    private void Awake()
    {
        transform.GetChild(0).gameObject.SetActive(true);
        //StartCoroutine(EnableChildern());
    }

    IEnumerator EnableChildern()
    {
        foreach(Transform child in transform)
        {
            child.gameObject.SetActive(true);
            yield return new WaitForSeconds(2);
        }
    }
}
