﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class PlayerImage : MonoBehaviour
{
    public List<Toggle> AvatarsImages;
    public int CounterAddition = 2;
    public string ImageUrl { get; set; }

    private void OnEnable()
    {
        StorageManager.Storage.ImageUploaded.AddListener(OnImageUploaded);
    }

    private void OnImageUploaded(string imageUrl)
    {
        AppManager.App.CurrentUser.Image.ImageUrl = imageUrl;
        AppManager.App.CurrentUser.Image.ImageExtention = Path.GetExtension(imageUrl);
        AppManager.App.CreateUser();
    }

    public void OnCreateUser()
    {
        var selectedAvatar = AvatarsImages[0].group.ActiveToggles().FirstOrDefault();

        var avatarIndex = AvatarsImages.IndexOf(selectedAvatar);

        if (AppManager.App.CurrentUser.Image == null)
        {
            AppManager.App.CurrentUser.Image = new ImageParams();
        }

        if (avatarIndex == 0)
        {
            AppManager.App.CurrentUser.Image.AvatarIndex = -1;
            StorageManager.Storage.UploadPlayerImage(AppManager.App.CurrentUser.Id + Path.GetExtension(ImageUrl), ImageUrl);
        }
        else
        {
            AppManager.App.CurrentUser.Image.ImageUrl = avatarIndex.ToString();
            AppManager.App.CurrentUser.Image.AvatarIndex = avatarIndex + CounterAddition;
            AppManager.App.CreateUser();
        }
    }
}
