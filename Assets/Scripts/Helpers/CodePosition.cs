﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class CodePosition
{
    private enum Shapes { RightLeft, LeftRight, UpDown, DownUp }

    public static List<Vector2> GetCodePosition(int codeLength, int columnsCount, int RowsCount)
    {
        var shape = GetShape();

        switch (shape)
        {
            case Shapes.RightLeft:
                return GetRightLeftCode(codeLength, columnsCount, RowsCount);
            case Shapes.LeftRight:
                return GetLefRightCode(codeLength, columnsCount, RowsCount);
            case Shapes.UpDown:
                return GetUpDownCode(codeLength, columnsCount, RowsCount);
            case Shapes.DownUp:
                return GetDownUpCode(codeLength, columnsCount, RowsCount);
            default:
                return new List<Vector2>();
        }
    }

    private static Shapes GetShape()
    {
        var shapesValues = Enum.GetValues(typeof(Shapes));
        return (Shapes)shapesValues.GetValue(Random.Range(0, shapesValues.Length));

        
    }

    private static List<Vector2> GetRightLeftCode(int codeLength, int columnsCount, int RowsCount)
    {
        var columnIndex = Random.Range(codeLength - 1, columnsCount);
        var rowIndex = Random.Range(0, RowsCount);

        var codePositions = new List<Vector2>();

        for (int i = 0; i < codeLength; i++)
        {
            codePositions.Add(new Vector2(columnIndex - i, rowIndex));
        }

        return codePositions;
    }

    private static List<Vector2> GetLefRightCode(int codeLength, int columnsCount, int RowsCount)
    {
        var columnIndex = Random.Range(0, columnsCount - codeLength + 1);
        var rowIndex = Random.Range(0, RowsCount);

        var codePositions = new List<Vector2>();

        for (int i = 0; i < codeLength; i++)
        {
            codePositions.Add(new Vector2(columnIndex + i, rowIndex));
        }

        return codePositions;
    }

    private static List<Vector2> GetUpDownCode(int codeLength, int columnsCount, int RowsCount)
    {
        var columnIndex = Random.Range(0, columnsCount);
        var rowIndex = Random.Range(0, RowsCount - codeLength + 1);

        var codePositions = new List<Vector2>();

        for (int i = 0; i < codeLength; i++)
        {
            codePositions.Add(new Vector2(columnIndex, rowIndex + i));
        }

        return codePositions;
    }

    private static List<Vector2> GetDownUpCode(int codeLength, int columnsCount, int RowsCount)
    {
        var columnIndex = Random.Range(0, columnsCount);
        var rowIndex = Random.Range(codeLength - 1, RowsCount);

        var codePositions = new List<Vector2>();

        for (int i = 0; i < codeLength; i++)
        {
            codePositions.Add(new Vector2(columnIndex, rowIndex - i));
        }

        return codePositions;
    }
}
