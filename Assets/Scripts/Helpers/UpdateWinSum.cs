using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UpdateWinSum : MonoBehaviour
{
    [TextArea]
    public string Template;

    private void OnEnable()
    {
        var text = GetComponent<TextMeshProUGUI>();
        if (text != null && AppManager.App.Reward != null)
        {
            text.text = string.Format(Template, AppManager.App.Reward.ConisToTransfer.ToString("N0"));
        }
    }
}
