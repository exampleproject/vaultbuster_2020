using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UpdateOpponentNameText : MonoBehaviour
{
    [TextArea]
    public string Template;

    private void OnEnable()
    {
        var text = GetComponent<TextMeshProUGUI>();
        if (text != null && AppManager.App.OpponentPlayer != null)
        {
            text.text = string.Format(Template, AppManager.App.OpponentPlayer.Name);
        }
    }
}
