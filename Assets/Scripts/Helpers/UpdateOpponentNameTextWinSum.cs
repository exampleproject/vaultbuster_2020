using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UpdateOpponentNameTextWinSum : MonoBehaviour
{
    [TextArea]
    public string Template;

    private void OnEnable()
    {
        var text = GetComponent<TextMeshProUGUI>();
        if (text != null && AppManager.App.OpponentPlayer != null && AppManager.App.Reward != null)
        {
            text.text = string.Format(Template, AppManager.App.OpponentPlayer.Name, AppManager.App.Reward.ConisToTransfer.ToString("N0"));
        }
    }
}
