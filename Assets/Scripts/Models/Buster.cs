public class Buster
{
    public string SenderId { get; set; }
    public string SenderName { get; set; }
    public bool IsBot { get; set; }
}
