using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Screenshot : MonoBehaviour
{
    private ParticleSystem _particleSystem;
    private int _counter;

    private void Awake()
    {
        _particleSystem = GetComponent<ParticleSystem>();
    }

    private void Start()
    {
        StartCoroutine(EndFrameScreenshot());
    }
    IEnumerator EndFrameScreenshot()
    {
        while (_particleSystem.isPlaying)
        {
            yield return new WaitForEndOfFrame();
            Texture2D screenShot;
            Vector2 screenshotTextureSize = new Vector2(992, 626);
            Rect screenshotBounds = new Rect(0, 0, 992, 626);

            screenShot = new Texture2D(Mathf.FloorToInt(screenshotTextureSize.x), Mathf.FloorToInt(screenshotTextureSize.y));
            screenShot.ReadPixels(screenshotBounds, 0, 0);
            screenShot.Apply();

            byte[] bytes = screenShot.EncodeToPNG();

            string myFileName = "D:/Sraya/Unity/Playtiz/Assets/Screenshot/screenshot_" + _counter + ".png";

            File.WriteAllBytes(myFileName, bytes);
            _counter++;
        }
    }
}
