﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class DirectionArrow : OpenDialog
{
    public CodeShapes Shape;

    //public float ApearTime = 0.5f;
    public float ShowTime = 1.5f;
    //public float FadoutTime = 0.25f;
    //public int ApearSpins = 5;

    protected override void OnEnable()
    {
        //StartCoroutine(ArrowAnimation());
        float rotate = 0;
        switch (Shape)
        {
            case CodeShapes.Right:
                rotate = 270;
                break;
            case CodeShapes.Left:
                rotate = 90;
                break;
            case CodeShapes.Up:
                rotate = 0;
                break;
            case CodeShapes.Down:
                rotate = 180;
                break;
            case CodeShapes.RightUp:
                rotate = 315;
                break;
            case CodeShapes.RightDown:
                rotate = 225;
                break;
            case CodeShapes.LeftUp:
                rotate = 45;
                break;
            case CodeShapes.LeftDown:
                rotate = 135;
                break;
            case CodeShapes.RightUpRight:
                rotate = 270;
                break;
            case CodeShapes.RightDownRight:
                rotate = 270;
                break;
            case CodeShapes.LeftUpLeft:
                rotate = 90;
                break;
            case CodeShapes.LeftDownLeft:
                rotate = 90;
                break;
            case CodeShapes.UpRightUp:
                rotate = 0;
                break;
            case CodeShapes.UpLeftUp:
                rotate = 0;
                break;
            case CodeShapes.DownRightDown:
                rotate = 180;
                break;
            case CodeShapes.DownLeftDown:
                rotate = 180;
                break;
            case CodeShapes.ZigzagRightUp:
                rotate = 315;
                break;
            case CodeShapes.ZigzagRightDown:
                rotate = 225;
                break;
            case CodeShapes.ZigzagLeftUp:
                rotate = 45;
                break;
            case CodeShapes.ZigzagLeftDown:
                rotate = 135;
                break;
            case CodeShapes.ZigzagUpRight:
                rotate = 315;
                break;
            case CodeShapes.ZigzagUpLeft:
                rotate = 45;
                break;
            case CodeShapes.ZigzagDownRight:
                rotate = 225;
                break;
            case CodeShapes.ZigzagDownLeft:
                rotate = 135;
                break;
        }
        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, rotate);
        base.OnEnable();
        StartCoroutine(WaitBeforeClose());
    }

    //private IEnumerator ArrowAnimation()
    //{
    //    var image = GetComponent<Image>();
    //    transform.localScale = Vector3.zero;
    //    float rotate = 0;
    //    switch (Shape)
    //    {
    //        case CodeShapes.Right:
    //            rotate = 270;
    //            break;
    //        case CodeShapes.Left:
    //            rotate = 90;
    //            break;
    //        case CodeShapes.Up:
    //            rotate = 0;
    //            break;
    //        case CodeShapes.Down:
    //            rotate = 180;
    //            break;
    //        case CodeShapes.RightUp:
    //            rotate = 315;
    //            break;
    //        case CodeShapes.RightDown:
    //            rotate = 225;
    //            break;
    //        case CodeShapes.LeftUp:
    //            rotate = 45;
    //            break;
    //        case CodeShapes.LeftDown:
    //            rotate = 135;
    //            break;
    //        case CodeShapes.RightUpRight:
    //            rotate = 270;
    //            break;
    //        case CodeShapes.RightDownRight:
    //            rotate = 270;
    //            break;
    //        case CodeShapes.LeftUpLeft:
    //            rotate = 90;
    //            break;
    //        case CodeShapes.LeftDownLeft:
    //            rotate = 90;
    //            break;
    //        case CodeShapes.UpRightUp:
    //            rotate = 0;
    //            break;
    //        case CodeShapes.UpLeftUp:
    //            rotate = 0;
    //            break;
    //        case CodeShapes.DownRightDown:
    //            rotate = 180;
    //            break;
    //        case CodeShapes.DownLeftDown:
    //            rotate = 180;
    //            break;
    //        case CodeShapes.ZigzagRightUp:
    //            rotate = 315;
    //            break;
    //        case CodeShapes.ZigzagRightDown:
    //            rotate = 225;
    //            break;
    //        case CodeShapes.ZigzagLeftUp:
    //            rotate = 45;
    //            break;
    //        case CodeShapes.ZigzagLeftDown:
    //            rotate = 135;
    //            break;
    //        case CodeShapes.ZigzagUpRight:
    //            rotate = 315;
    //            break;
    //        case CodeShapes.ZigzagUpLeft:
    //            rotate = 45;
    //            break;
    //        case CodeShapes.ZigzagDownRight:
    //            rotate = 225;
    //            break;
    //        case CodeShapes.ZigzagDownLeft:
    //            rotate = 135;
    //            break;
    //    }

    //    for (float timer = 0; timer <= ApearTime; timer += Time.deltaTime)
    //    {
    //        var percent = timer / ApearTime;
    //        transform.localScale = new Vector3(percent, percent, percent);
    //        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, rotate + 360 * ApearSpins * percent);
    //        yield return null;
    //    }
    //    transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, rotate);
    //    transform.localScale = Vector3.one;

    //    yield return new WaitForSecondsRealtime(ShowTime);

    //    for (float timer = FadoutTime; timer > 0; timer -= Time.deltaTime)
    //    {
    //        image.color = new Color(image.color.r, image.color.g, image.color.b, timer / FadoutTime);
    //    }
    //    image.color = new Color(image.color.r, image.color.g, image.color.b, 0);

    //    gameObject.SetActive(false);
    //}

    //private void OnDisable()
    //{
    //    var image = GetComponent<Image>();
    //    image.color = new Color(image.color.r, image.color.g, image.color.b, 1);
    //}

    private IEnumerator WaitBeforeClose()
    {
        var openTime = Animations.Where(a => a.AnimationName == "Scale").Select(a => a.AnimationTime).FirstOrDefault();
        yield return new WaitForSeconds(ShowTime + openTime);
        OnClose();
    }
}
