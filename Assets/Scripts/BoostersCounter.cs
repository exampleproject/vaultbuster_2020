using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BoostersCounter : MonoBehaviour
{
    private TextMeshProUGUI _boosterCounterText;
    private string _text = "<mark=#000000 padding=\"60, 60, 0, 0\"><font=\"LiberationSans SDF - Fallback\">Boosters left ";

    private void Awake()
    {
        _boosterCounterText = GetComponent<TextMeshProUGUI>();
    }

    private void OnEnable()
    {
        UpdateCounter();
    }

    public void UpdateCounter()
    {
        _boosterCounterText.text = _text + AppManager.App.CurrentUser.Vault.Boosters;
    }
}
