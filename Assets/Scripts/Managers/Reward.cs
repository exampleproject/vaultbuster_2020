﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reward : MonoBehaviour
{
    public bool IsNewXP { get; set; }

    private bool _isDegreePopup;
    private bool _isWorldPopup;

    public void OnOKClick(bool isBustAgain)
    {
        if (IsNewXP)
        {
            var currentXP = AppManager.App.CurrentUser.XP;
            var lastXP = AppManager.App.CurrentUser.XP - 1;
            var gameProgress = GameProgressConfig.GameProgress.GameProgressData;
            if (gameProgress[currentXP].Degree != gameProgress[lastXP].Degree && !_isDegreePopup)
            {
                UIReferences.References.ActiveScreen(ScreensManager.Screens.DEGREE_POPUP, ScreensManager.Requires.NONE);
                _isDegreePopup = true;
                return;
            }
            if (gameProgress[currentXP].World != gameProgress[lastXP].World && !_isWorldPopup)
            {
                UIReferences.References.ActiveScreen(ScreensManager.Screens.WORLD_POPUP, ScreensManager.Requires.NONE);
                _isWorldPopup = true;
                return;
            }

            if (isBustAgain && AppManager.App.CheckEnoughAmount(DatabaseManager.UserDataKeys.Games))
            {
                UIReferences.References.IsAfterGame = true;
                UIReferences.References.ActiveScreen(ScreensManager.Screens.CHOOSE_PLAYER, ScreensManager.Requires.NONE);
                return;
            }
            UIReferences.References.IsAfterGame = true;
            UIReferences.References.ActiveScreen(ScreensManager.Screens.CHOOSE_SKILL_LEVEL, ScreensManager.Requires.GAME_PROGRESS);
            return;
        }
        else
        {
            _isDegreePopup = false;
            _isWorldPopup = false;
        }
        
        if (isBustAgain && AppManager.App.CheckEnoughAmount(DatabaseManager.UserDataKeys.Games))
        {
            UIReferences.References.IsAfterGame = true;
            UIReferences.References.ActiveScreen(ScreensManager.Screens.CHOOSE_PLAYER, ScreensManager.Requires.NONE);
            return;
        }
        UIReferences.References.IsAfterGame = true;
        UIReferences.References.ActiveScreen(ScreensManager.Screens.CHOOSE_SKILL_LEVEL, ScreensManager.Requires.GAME_PROGRESS);
    }
}
