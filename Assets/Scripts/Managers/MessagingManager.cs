﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Messaging;
using System;
using Firebase;
using Firebase.Extensions;
using Firebase.Analytics;
using System.Globalization;
using UnityEngine.Events;
using System.Threading.Tasks;

public class MessagingManager : MonoBehaviour
{
    public UnityEvent FirebaseDependencies;

    public UnityEventBuster UnderAttack;
    public UnityEvent CodeSwiped;
    public static MessagingManager Messaging;

    private bool _isNotification;
    public bool IsNotification
    {
        get
        {
            return _isNotification;
        }
        set
        {
            if (_isNotification != value)
            {
                _isNotification = value;
            }
        }
    }

    private void Awake()
    {
        if (Messaging == null)
        {
            Messaging = this;
            StartFirebase();
            return;
        }
        Destroy(gameObject);
    }

    public void StartFirebase()
    {
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWithOnMainThread(task =>
        {
            var dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                //InitializeFirebase();
                FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
                FirebaseDependencies.Invoke();
                OnFirebaseDependencies();
            }
            else
            {
                Debug.LogError("Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });
    }

    private void OnFirebaseDependencies()
    {
        Debug.Log("Messaging.................");
        FirebaseMessaging.TokenReceived += OnTokenReceived;
        FirebaseMessaging.MessageReceived += OnMessageReceived;
        FirebaseMessaging.SubscribeAsync("topic").ContinueWithOnMainThread(task =>
        {
            Debug.Log("subcribeAsync");
        });
        FirebaseMessaging.RequestPermissionAsync().ContinueWithOnMainThread(task =>
        {
            Debug.Log("RequestPermissionAsync");
        });
    }

    private void OnTokenReceived(object sender, TokenReceivedEventArgs token)
    {
        DatabaseManager.Database.SetTokens(token.Token);
    }

    private void OnMessageReceived(object sender, MessageReceivedEventArgs e)
    {
        try
        {
            string messageType;

            if (!e.Message.Data.TryGetValue("type", out messageType))
            {
                return;
            }
            switch (messageType)
            {
                case "attack-alarm":
                    string endTimeString, senderID, senderName, isBot;
                    if (!e.Message.Data.TryGetValue("endTime", out endTimeString) || !e.Message.Data.TryGetValue("senderID", out senderID) || !e.Message.Data.TryGetValue("senderName", out senderName) || !e.Message.Data.TryGetValue("isBot", out isBot))
                    {
                        break;
                    }
                    DateTime endTime;
                    if (DateTime.TryParseExact(endTimeString, UIReferences.DATE_FORMAT, CultureInfo.InvariantCulture, DateTimeStyles.AllowInnerWhite, out endTime))
                    {
                        AppManager.App.EndAttackTime = endTime;
                    }
                    UnderAttack.Invoke(new Buster
                    {
                        SenderId = senderID,
                        SenderName = senderName,
                        IsBot = bool.Parse(isBot)
                    });
                    break;
                case "change-code":
                    UIReferences.References.ActiveScreen(ScreensManager.Screens.CODE_SWIPED, ScreensManager.Requires.NONE);
                    CodeSwiped.Invoke();
                    break;
            }
        }
        catch (Exception ex)
        {
            Debug.Log("Message error " + ex.Message + " Trace: " + ex.StackTrace);
        }
    }
}
