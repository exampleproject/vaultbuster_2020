﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;
using Firebase.Auth;
using Firebase.Analytics;

public class AppManager : MonoBehaviour
{
    public bool IsDebugMode;
    public IAPManager IAP;
    public Image TestImage;

    public bool IsGotReward { get; set; }

    public static AppManager App;

    public User CurrentUser { get; private set; }
    public Buster AttackBuster { get; private set; }
    public PlayerData OpponentPlayer { get; set; }
    public DateTime EndAttackTime { get; set; }
    public int CurrentMinToBust { get; set; }
    public int CurrentMaxToBust { get; set; }
    public bool IsAttacking { get; set; }
    public List<string> SocialIds { get; private set; }
    public CoinsReward Reward { get; private set; }

    private string _userId;
    private bool _isStartGame;
    private bool _isSocialInit;
    private bool _isSetFriend;

    private void Awake()
    {
        if (App != null)
        {
            Destroy(gameObject);
            return;
        }
        App = this;
        LoginManager.Login.LoginID.AddListener(OnLogedIn);
        DatabaseManager.Database.GotUsers.AddListener(OnGotUsers);
        MessagingManager.Messaging.UnderAttack.AddListener(OnUnderAttack);
        StorageManager.Storage.SpriteDownloaded.AddListener(OnSpriteDownloaded);
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        //Social
    }

    private void OnSpriteDownloaded(string id, Sprite sprite)
    {
        if (CurrentUser.Id == id)
        {
            UIReferences.References.Menu.ProfileElements.UserImage.sprite = sprite;
            StorageManager.Storage.SpriteDownloaded.RemoveListener(OnSpriteDownloaded);
        }
    }

    private void OnUnderAttack(Buster buster)
    {
        AttackBuster = buster;
    }

    private void OnLogedIn(FirebaseUser user)
    {
        if (UIReferences.References.GameFllow.FirstScreen.gameObject.activeInHierarchy)
        {
            UIReferences.References.GameFllow.FirstScreen.gameObject.SetActive(false);
        }
        if (UIReferences.References.Login.EmailLoginScreen.gameObject.activeInHierarchy)
        {
            UIReferences.References.Login.EmailLoginScreen.gameObject.SetActive(false);
        }
        UIReferences.References.GameFllow.WaitWheel.gameObject.SetActive(true);
        _userId = user.UserId;

        if (LoginManager.Login.LoginPlatform == LoginManager.LoginPlatforms.Facebook)
        {
            CurrentUser = new User();
            CurrentUser.Id = _userId;

            CurrentUser.Name = user.DisplayName;
            CurrentUser.Image = new ImageParams();
            CurrentUser.Image.ImageUrl = user.PhotoUrl.ToString();
            CurrentUser.Image.AvatarIndex = -1;
        }

        DatabaseManager.Database.StartDatabase(_userId);
        StorageManager.Storage.StartStorage();
        FuncitionsManager.Functions.StartFunctionsManager();
        FuncitionsManager.Functions.GotCoinsFromBreakin.AddListener(OnGotCoinsFromBreakin);
    }

    private void OnGotUsers(User user)
    {
        if (UIReferences.References.GameFllow.WaitWheel.gameObject.activeInHierarchy)
        {
            UIReferences.References.GameFllow.WaitWheel.gameObject.SetActive(false);
        }
        if (user == null)
        {
            if (LoginManager.Login.LoginPlatform == LoginManager.LoginPlatforms.Facebook)
            {
                CreateUser();
            }
            else
            {
                CurrentUser = new User();
                CurrentUser.Id = _userId;
                UIReferences.References.ActiveScreen(ScreensManager.Screens.NEW_USER_SETTINGS, ScreensManager.Requires.NONE);
                _isStartGame = true;
            }
        }
        else
        {
            CurrentUser = user;

            DatabaseManager.Database.UpdateTokens();

            Debug.Log("Is Under Attack: " + CurrentUser.IsUnderAttack);
            CheckAttackTime();

            if (user.Image.AvatarIndex == -1)
            {
                StorageManager.Storage.DownloadPlayerImage(user.Id, user.Image.ImageUrl);
            }
            else
            {
                UIReferences.References.Menu.ProfileElements.UserImage.sprite = Resources.LoadAll<Sprite>("Sprites/avatars")[user.Image.AvatarIndex];
            }

            if (CurrentUser.IsUnderAttack == "true")
            {
                UIReferences.References.ActiveScreen(ScreensManager.Screens.REACT, ScreensManager.Requires.NONE);
            }
            else if (!_isStartGame)
            {
                if (!UIReferences.References.TopBar.TopBarPanel.gameObject.activeInHierarchy)
                {
                    Debug.Log("Start");
                    UIReferences.References.TopBar.TopBarPanel.gameObject.SetActive(true);
                }
                UIReferences.References.ActiveScreen(ScreensManager.Screens.CHOOSE_SKILL_LEVEL, ScreensManager.Requires.GAME_PROGRESS);
                _isStartGame = true;
            }
        }
        if (CurrentUser.IsNotifications != GameConfigManager.GameConfig.IsNotifications)
        {
            Debug.Log("Update Database Is Notification");
            DatabaseManager.Database.UpdateUserDataPrameters(new Dictionary<string, object> { { DatabaseManager.Database.GetPathFromKey(DatabaseManager.UserDataKeys.IsNotifications), GameConfigManager.GameConfig.IsNotifications.ToString().ToLower() } });
        }
        if (!_isSetFriend) //social
        {
            
        }

        IAP.gameObject.SetActive(true);
        LinksManager.links.StartLinks();
    }

    public void OnFirstSkip()
    {
        LoginManager.Login.FacebookSignOut();
#if UNITY_EDITOR
        LoginManager.Login.InitializeFirebaseForEmail();
#elif UNITY_ANDROID
        LoginManager.Login.InitAndroidAuth();

#endif
    }

    public void OnEmailLogin()
    {
        LoginManager.Login.CreateUserWithEmailAsync(UIReferences.References.Login.EmailField.text, UIReferences.References.Login.PasswordField.text);
    }

    public void OnChooseAvatar(bool isOn, int avatarIndex)
    {
        if (isOn)
        {
            CurrentUser.Image.AvatarIndex = avatarIndex;
        }
    }

    private void OnGotCoinsFromBreakin(CoinsReward coinsReward)
    {
        Reward = coinsReward;
        App.CurrentUser.Vault.Coins += coinsReward.ConisToTransfer;
        IsGotReward = true;
    }

    public void CreateUser()
    {
        if (string.IsNullOrEmpty(CurrentUser.Id))
        {
            CurrentUser.Id = _userId;
        }
        if (UIReferences.References.Login.UserSettingsScreen.gameObject.activeInHierarchy)
        {
            CurrentUser.Name = UIReferences.References.Login.NameField.text;
        }
        CurrentUser.Vault = new VaultParams();
        CurrentUser.Vault.GoldBars = 0;
        CurrentUser.Vault.Coins = 0;
        CurrentUser.Vault.CoinsPercentsCanTaken = 80;
        CurrentUser.Vault.Assets = new List<string>();
        CurrentUser.Vault.Games = 0;
        CurrentUser.Vault.Boosters = 0;
        CurrentUser.Vault.Shields = 0;
        CurrentUser.IsUnderAttack = "false";
        CurrentUser.TokensList = new List<string>();
        CurrentUser.UserGamesProgress = new UserGamesProgressData();
        CurrentUser.UserGamesProgress.TotalGames = 0;
        CurrentUser.UserGamesProgress.TotalWins = 0;
        CurrentUser.UserGamesProgress.CurrentGames = 0;
        CurrentUser.UserGamesProgress.CurrentWins = 0;
        CurrentUser.XP = 0;
        DatabaseManager.Database.AddUser();
        UIReferences.References.ActiveScreen(ScreensManager.Screens.WELLCOM_BONUSES, ScreensManager.Requires.NONE);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
        if (CurrentUser != null)
        {
            if (CurrentUser.Vault.Games == 0)
            {
                if (string.IsNullOrEmpty(CurrentUser.NextCharge))
                {
                    DatabaseManager.Database.UpdateUserDataPrameters(new Dictionary<string, object> { { DatabaseManager.Database.GetPathFromKey(DatabaseManager.UserDataKeys.NextCharge), (DateTime.Now.AddHours(1)).ToString(UIReferences.DATE_FORMAT) } });
                }
                else
                {
                    DateTime nextCharge;
                    if (DateTime.TryParseExact(CurrentUser.NextCharge, UIReferences.DATE_FORMAT, CultureInfo.InvariantCulture, DateTimeStyles.AllowInnerWhite, out nextCharge) && nextCharge <= DateTime.Now)
                    {
                        UIReferences.References.ActiveScreen(ScreensManager.Screens.RECHARGE, ScreensManager.Requires.NONE);
                    }
                }
            }
            else if(!string.IsNullOrEmpty(CurrentUser.NextCharge))
            {
                CurrentUser.NextCharge = string.Empty;
                DatabaseManager.Database.UpdateUserDataPrameters(new Dictionary<string, object> { { DatabaseManager.Database.GetPathFromKey(DatabaseManager.UserDataKeys.NextCharge), string.Empty } });
            }
        }
    }

    private void CheckAttackTime()
    {
        if (CurrentUser.IsUnderAttack == "true")
        {
            if (EndAttackTime != null && EndAttackTime <= DateTime.Now)
            {
                CurrentUser.IsUnderAttack = "false";
                DatabaseManager.Database.UpdateUserDataPrameters(new Dictionary<string, object> { { DatabaseManager.Database.GetPathFromKey(DatabaseManager.UserDataKeys.IsUnderAttack), false } });
            }
        }
    }

    public void RemoveRevenge(Revenge revenge)
    {
        CurrentUser.RevengesList.Remove(revenge);
        DatabaseManager.Database.UpdateRevenges();
    }

    public void TestInvite() //Social
    {
        LinksManager.links.ShortLink();
    }

    public bool CheckEnoughAmount(DatabaseManager.UserDataKeys key, int amountNeeded = 1)
    {
        switch (key)
        {
            case DatabaseManager.UserDataKeys.GoldBars:
                return CurrentUser.Vault.GoldBars >= amountNeeded;
            case DatabaseManager.UserDataKeys.Coins:
                return CurrentUser.Vault.Coins >= amountNeeded;
            case DatabaseManager.UserDataKeys.Games:
                return CurrentUser.Vault.Games >= amountNeeded;
            case DatabaseManager.UserDataKeys.Boosters:
                return CurrentUser.Vault.Boosters >= amountNeeded;
            case DatabaseManager.UserDataKeys.Shields:
                return CurrentUser.Vault.Shields >= amountNeeded;
            default:
                return true;
        }
    }
}