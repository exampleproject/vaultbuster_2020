using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GameConfigManager : MonoBehaviour
{
    public string FileName = "VaultBusterConfig.config";

    public bool IsMusic
    {
        set
        {
            if (_config.IsMusic != value)
            {
                _config.IsMusic = value;
                SetGameConfig(_config);
            }
        }
        get
        {
            return _config.IsMusic;
        }
    }

    public bool IsSoundEffects
    {
        set
        {
            if (_config.IsSoundEffects != value)
            {
                _config.IsSoundEffects = value;
                SetGameConfig(_config);
            }
        }
        get
        {
            return _config.IsSoundEffects;
        }
    }

    public bool IsNotifications
    {
        set
        {
            if (_config.IsNotifications != value)
            {
                _config.IsNotifications = value;
                SetGameConfig(_config);
                DatabaseManager.Database.UpdateUserDataPrameters(new Dictionary<string, object> { { DatabaseManager.Database.GetPathFromKey(DatabaseManager.UserDataKeys.IsNotifications), value.ToString().ToLower() } });
            }
        }
        get
        {
            return _config.IsNotifications;
        }
    }

    public int ShieldsNotificationsCounter
    {
        set
        {
            if (_config.ShieldsNotificationsCounter != value)
            {
                _config.ShieldsNotificationsCounter = value;
                SetGameConfig(_config);
            }
        }
        get
        {
            return _config.ShieldsNotificationsCounter;
        }
    }

    public int LowGoldBarsNotificationCounter
    {
        set
        {
            if (_config.LowGoldBarsNotificationCounter != value)
            {
                _config.LowGoldBarsNotificationCounter = value;
                SetGameConfig(_config);
            }
        }
        get
        {
            return _config.LowGoldBarsNotificationCounter;
        }
    }

    private GameConfigObject _config;

    public static GameConfigManager GameConfig;

    private void Awake()
    {
        if (GameConfig != null)
        {
            return;
        }
        GameConfig = this;
        _config = GetGameConfig();
        UIReferences.References.Menu.MusicSettings.isOn = _config.IsMusic;
        UIReferences.References.Menu.SoundEffectsSettings.isOn = _config.IsSoundEffects;
        UIReferences.References.Menu.NotificationsSettings.isOn = _config.IsNotifications;
    }

    private GameConfigObject GetGameConfig()
    {
        var configText = ReadConfigFile();
        if (string.IsNullOrEmpty(configText))
        {
            var configObject = new GameConfigObject
            {
                IsMusic = true,
                IsSoundEffects = true,
                IsNotifications = true
            };
            SetGameConfig(configObject);
            return configObject;
        }
        return JsonUtility.FromJson<GameConfigObject>(configText);
    }

    private string ReadConfigFile()
    {
        var file = Application.persistentDataPath + "/" + FileName;
        if (!File.Exists(file))
        {
            return string.Empty;
        }
        var reader = File.OpenText(file);
        var text = reader.ReadToEnd();
        reader.Close();
        return text;
    }

    private void SetGameConfig(GameConfigObject configObject)
    {
        WriteConfigFile(JsonUtility.ToJson(configObject));
    }

    private void WriteConfigFile(string text)
    {
        var file = Application.persistentDataPath + "/" + FileName;
        Debug.Log(file);
        File.WriteAllText(file, text);
    }
}

[Serializable]
public class GameConfigObject
{
    public bool IsMusic;
    public bool IsSoundEffects;
    public bool IsNotifications;
    public int LowGoldBarsNotificationCounter;
    public int ShieldsNotificationsCounter;
}