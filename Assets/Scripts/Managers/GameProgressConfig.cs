﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameProgressConfig : MonoBehaviour
{
    public List<XPData> GameProgressData;
    public static GameProgressConfig GameProgress;

    private int _choosenXPData;

    private void Awake()
    {
        if (GameProgress == null)
        {
            GameProgress = this;
            return;
        }
        Destroy(GameProgress);
    }

    public List<int> GetUpTo()
    {
        List<int> upTo = new List<int>();
        foreach(var xp in GameProgressData)
        {
            if (!upTo.Contains(xp.UpTo))
            {
                upTo.Add(xp.UpTo);
            }
        }
        return upTo;
    }

    public XPData GetCurrentXPData()
    {
        return GameProgressData[AppManager.App.CurrentUser.XP];
    }

    public void SetChoosenIndex(int choosenIndex)
    {
        _choosenXPData = choosenIndex;
    }

    public void SetChoosenIndexByUpTo(int upTo)
    {
        var choosenXPData = GameProgressData.Where(x => x.UpTo == upTo).ToList().LastOrDefault();
        _choosenXPData = GameProgressData.IndexOf(choosenXPData);
    }

    public XPData GetChoosenXPData()
    {
        return GameProgressData[_choosenXPData];
    }
}

[Serializable]
public class XPData
{
    public int World;
    public int UpTo;
    public string Directions;
    public int TotalGamesTime;
    public int RefreshSeconds;
    public int CodeDigits;
    public int Variations;
    public int MinTotalGames;
    public int SuccessiveWin;
    public string Degree;
    public string MinCoinsToSpent;
}
