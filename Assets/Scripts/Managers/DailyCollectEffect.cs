using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DailyCollectEffect : MonoBehaviour
{
    public ScreensManager.Screens NextScreen;
    public ScreensManager.Requires NextRequire;

    private void OnEnable()
    {
        NextScreen = ScreensManager.Screens.CHOOSE_SKILL_LEVEL;
        NextRequire = ScreensManager.Requires.GAME_PROGRESS;
    }
    public void OnParticleSystemStopped()
    {
        if (NextScreen != ScreensManager.Screens.NONE)
        {
            UIReferences.References.ActiveScreen(NextScreen, NextRequire);
        }
        gameObject.SetActive(false);
    }
}
