﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.Playables;

public class UIReferences : MonoBehaviour
{
    public enum MessagesTypes { OutOfGamesMessage, OutOfGoldBarsMessage, NotEnoughGlodBarsMessage, OutOfBoostersMessage, OutOfShieldsMessage, ReminderMessage, Error }

    [Header("Canvas")]
    public GameFllowUI GameFllow;
    public TopbarUI TopBar;
    public LoginUI Login;
    public BreakInUI BreakIn;
    public ShopUI Shop;
    public DailyBonusUI DailyBonus;
    public ReactUI React;
    public MenuUI Menu;
    public MessagesUI Messages;
    public Text DebugText;
    public const string DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public bool IsAfterGame { get; set; }

    private ScreensManager.Screens _currentScreen;
    private ScreensManager.Screens _lastScreen;

    public static UIReferences References;

    private void Awake()
    {
        if (References == null)
        {
            References = this;
            return;
        }
        Destroy(gameObject);
    }

    private void Start()
    {
        Debug.Log(AppManager.App);
        _currentScreen = ScreensManager.Screens.WAIT_WHEEL;
    }

    public void DebugOnScreen(string debugMessage)
    {
        References.DebugText.text = debugMessage;
        if (!References.DebugText.gameObject.activeInHierarchy)
        {
            References.DebugText.gameObject.SetActive(true);
        }
    }

    public void HideDebugFromScreen()
    {
        References.DebugText.text = string.Empty;
        if (References.DebugText.gameObject.activeInHierarchy)
        {
            References.DebugText.gameObject.SetActive(false);
        }
    }

    public void Wait()
    {
        if (References.GameFllow.ChooseSkillLevelScreen.gameObject.activeInHierarchy)
        {
            References.GameFllow.ChooseSkillLevelScreen.gameObject.SetActive(false);
        }
        if (References.GameFllow.ChoosePlayerScreen.gameObject.activeInHierarchy)
        {
            References.GameFllow.ChoosePlayerScreen.gameObject.SetActive(false);
        }
        if (References.Login.UserSettingsScreen.gameObject.activeInHierarchy)
        {
            References.Login.UserSettingsScreen.gameObject.SetActive(false);
        }
        References.GameFllow.WaitWheel.gameObject.SetActive(true);
    }

    private IEnumerator WaitForGameProgressConfig(ScreensManager.Screens nextScreen)
    {
        ActiveScreen(ScreensManager.Screens.WAIT_WHEEL, ScreensManager.Requires.NONE);
        while (GameProgressConfig.GameProgress.GameProgressData.Count < 244 || !Trophies.TrophiesSprites.IsLoaded())
        {
            yield return null;
        }
        ActiveScreen(nextScreen, ScreensManager.Requires.NONE);
    }

    private IEnumerator WaitForDailyBonusConfig(ScreensManager.Screens nextScreen)
    {
        ActiveScreen(ScreensManager.Screens.WAIT_WHEEL, ScreensManager.Requires.NONE);
        var dailyBonusManager = References.GameFllow.DailyBonusScreen.GetComponentInParent<DailyBonusManager>();
        while (!dailyBonusManager.IsDailyBonusData())
        {
            yield return null;
        }
        ActiveScreen(nextScreen, ScreensManager.Requires.NONE);
    }

    private IEnumerator WaitForShopData(ScreensManager.Screens nextScreen)
    {
        ActiveScreen(ScreensManager.Screens.WAIT_WHEEL, ScreensManager.Requires.NONE);
        var shopManager = References.Shop.ShopCanvas.GetComponent<ShopManager>();
        while (!shopManager.IsShopData())
        {
            yield return null;
        }
        ActiveScreen(nextScreen, ScreensManager.Requires.NONE);
        References.Shop.ShopCanvas.GetComponent<ShopManager>().SetPrices();
    }

    private IEnumerator WaitForThrophies(ScreensManager.Screens nextScreen)
    {
        ActiveScreen(ScreensManager.Screens.WAIT_WHEEL, ScreensManager.Requires.NONE);
        while (!Trophies.TrophiesSprites.IsLoaded())
        {
            yield return null;
        }
        ActiveScreen(nextScreen, ScreensManager.Requires.NONE);
    }

    private IEnumerator WaitForCoinsReward(ScreensManager.Screens nextScreen)
    {
        ActiveScreen(ScreensManager.Screens.WAIT_WHEEL, ScreensManager.Requires.NONE);
        ScreensToGameObject(nextScreen).SetActive(true);
        FuncitionsManager.Functions.TransferCoins();
        while (!AppManager.App.IsGotReward)
        {
            yield return null;
        }
        AppManager.App.IsGotReward = false;
        //References.BreakIn.SucceessScreen.StartTransferEffect();
        ActiveScreen(nextScreen, ScreensManager.Requires.NONE);
    }

    public void ActiveScreen(ScreensManager.Screens newScreen, ScreensManager.Requires waitingTo)
    {
        if (newScreen == ScreensManager.Screens.CHOOSE_PLAYER && !AppManager.App.CheckEnoughAmount(DatabaseManager.UserDataKeys.Games))
        {
            References.ShowMessage(MessagesTypes.OutOfGamesMessage);
            return;
        }
        if (_currentScreen != newScreen)
        {
            if (newScreen == ScreensManager.Screens.WAIT_WHEEL)
            {
                ScreensToGameObject(ScreensManager.Screens.WAIT_WHEEL).SetActive(true);
                return;
            }
            else if (ScreensToGameObject(ScreensManager.Screens.WAIT_WHEEL).activeInHierarchy)
            {
                ScreensToGameObject(ScreensManager.Screens.WAIT_WHEEL).SetActive(false);
            }

            if (IsAfterGame)
            {
                IsAfterGame = false;
                if (AppManager.App.CurrentUser.Vault.Shields == 0)
                {
                    if (GameConfigManager.GameConfig.ShieldsNotificationsCounter <= 10)
                    {
                        if (GameConfigManager.GameConfig.ShieldsNotificationsCounter % 5 == 0)
                        {
                            ShowMessage(UIReferences.MessagesTypes.OutOfShieldsMessage);
                        }
                        GameConfigManager.GameConfig.ShieldsNotificationsCounter++;
                    }
                }
                else if (GameConfigManager.GameConfig.ShieldsNotificationsCounter != 0)
                {
                    GameConfigManager.GameConfig.ShieldsNotificationsCounter = 0;
                }
                if (AppManager.App.CurrentUser.Vault.Games == 0)
                {
                    ShowMessage(UIReferences.MessagesTypes.OutOfGamesMessage);
                }
                if (AppManager.App.CurrentUser.Vault.Boosters == 0)
                {
                    ShowMessage(UIReferences.MessagesTypes.OutOfBoostersMessage);
                }
            }

            switch (waitingTo)
            {
                case ScreensManager.Requires.NONE:
                    break;
                case ScreensManager.Requires.GAME_PROGRESS:
                    StartCoroutine(WaitForGameProgressConfig(newScreen));
                    return;
                case ScreensManager.Requires.DAILY_BONUS:
                    StartCoroutine(WaitForDailyBonusConfig(newScreen));
                    return;
                case ScreensManager.Requires.SHOP:
                    StartCoroutine(WaitForShopData(newScreen));
                    return;
                case ScreensManager.Requires.TROPHIES:
                    StartCoroutine(WaitForThrophies(newScreen));
                    return;
                case ScreensManager.Requires.COINS_REWARD:
                    StartCoroutine(WaitForCoinsReward(newScreen));
                    return;
            }

            _lastScreen = _currentScreen;
            _currentScreen = newScreen;

            var openDialog = ScreensToGameObject(_lastScreen).GetComponent<OpenDialog>();
            if (openDialog == null)
            {
                ScreensToGameObject(_lastScreen).SetActive(false);
            }
            else
            {
                openDialog.OnClose();
            }

            if (!ScreensToGameObject(_currentScreen).activeInHierarchy)
            {
                ScreensToGameObject(_currentScreen).SetActive(true);
            }
            if (newScreen == ScreensManager.Screens.CHOOSE_PLAYER)
            {
                References.GameFllow.WaitWheel.gameObject.SetActive(true);
                ScreensToGameObject(ScreensManager.Screens.WAIT_WHEEL).SetActive(true);
                ScreensToGameObject(_currentScreen).GetComponentInChildren<ChoosePlayer>().GetOpponents(AppManager.App.CurrentMinToBust, AppManager.App.CurrentMaxToBust);
            }
        }
    }

    public void ActiveScreen()
    {
        var screens = EventSystem.current.currentSelectedGameObject.GetComponent<ScreensManager>();
        if (screens == null)
        {
            return;
        }

        ActiveScreen(screens.NextScreen, screens.WaitingTo);
    }

    public void ActiveLastScreen()
    {
        ActiveScreen(_lastScreen, ScreensManager.Requires.NONE);
    }

    private GameObject ScreensToGameObject(ScreensManager.Screens s)
    {
        switch (s)
        {
            case ScreensManager.Screens.FIRST_SCREEN:
                return References.GameFllow.FirstScreen.gameObject;
            case ScreensManager.Screens.NEW_USER_SETTINGS:
                return References.Login.UserSettingsScreen.gameObject;
            case ScreensManager.Screens.WELLCOM_BONUSES:
                return References.Login.WellcomeBonusesDialog.gameObject;
            case ScreensManager.Screens.CHOOSE_SKILL_LEVEL:
                return References.GameFllow.ChooseSkillLevelScreen.gameObject;
            case ScreensManager.Screens.CHOOSE_PLAYER:
                return References.GameFllow.ChoosePlayerScreen.gameObject;
            case ScreensManager.Screens.BREAKIN:
                return References.BreakIn.BreakInScreen.gameObject;
            case ScreensManager.Screens.TRANSFER_EFFECT:
                return References.BreakIn.SucceessScreen.gameObject;
            case ScreensManager.Screens.REWARD:
                return References.BreakIn.RewardScreen.gameObject;
            case ScreensManager.Screens.REWARD_OPTIONS:
                return References.BreakIn.RewardOptions.gameObject;
            case ScreensManager.Screens.DEGREE_POPUP:
                return References.BreakIn.DegreePopup.gameObject;
            case ScreensManager.Screens.WORLD_POPUP:
                return References.BreakIn.WorldPopup.gameObject;
            case ScreensManager.Screens.WORLD_HIGHER_POPUP:
                return References.BreakIn.WorldHigherPopup.gameObject;
            case ScreensManager.Screens.DIRECTIONS:
                return References.GameFllow.DirectionsScreen.gameObject;
            case ScreensManager.Screens.CODE_SWIPED:
                return References.BreakIn.CodeSwipedScreen.gameObject;
            case ScreensManager.Screens.TIME_OUT:
                return References.BreakIn.TimeOutScreen.gameObject;
            case ScreensManager.Screens.SHOP:
                return References.Shop.ShopScreen.gameObject;
            case ScreensManager.Screens.DAILY_BONUS_WHEEL:
                return References.DailyBonus.DailyBonusWheelScreen.gameObject;
            case ScreensManager.Screens.DAILY_BONUS_COLLECT:
                return References.DailyBonus.DailyBonusCollectScreen.gameObject;
            case ScreensManager.Screens.REACT:
                return References.React.ReactScreen.gameObject;
            case ScreensManager.Screens.WAIT_WHEEL:
                return References.GameFllow.WaitWheel.gameObject;
            case ScreensManager.Screens.RECHARGE:
                return References.GameFllow.RechargeScreen.gameObject;
            default:
                return References.Login.UserSettingsScreen.gameObject;
        }
    }

    public void ShowMessage(MessagesTypes message)
    {
        switch (message)
        {
            case MessagesTypes.OutOfGamesMessage:
                Messages.OutOfGamesMessage.gameObject.SetActive(true);
                return;
            case MessagesTypes.OutOfGoldBarsMessage:
                Messages.OutOfGoldBarsMessage.gameObject.SetActive(true);
                return;
            case MessagesTypes.NotEnoughGlodBarsMessage:
                Messages.NotEnoughGlodBarsMessage.gameObject.SetActive(true);
                return;
            case MessagesTypes.OutOfBoostersMessage:
                Messages.OutOfBoostersMessage.gameObject.SetActive(true);
                return;
            case MessagesTypes.OutOfShieldsMessage:
                Messages.OutOfShieldsMessage.gameObject.SetActive(true);
                return;
            case MessagesTypes.ReminderMessage:
                Messages.ReminderMessage.gameObject.SetActive(true);
                return;
            case MessagesTypes.Error:
                Messages.ErrorMessage.gameObject.SetActive(true);
                return;
        }
    }
}

[Serializable]
public class GameFllowUI
{
    public Canvas GameFllowCanvas;
    public RectTransform FirstScreen;
    public Image ChooseSkillLevelScreen;
    public RectTransform ChoosePlayerScreen;
    public Image ChoosePlayerBackground;
    public RectTransform NoRevengeScreen;
    public RectTransform RevengeScreen;
    public GridLayoutGroup RevengeContainer;
    public RectTransform NoFriendsScreen;
    public RectTransform FriendsScreen;
    public DirectionsPanel DirectionsScreen;
    public Image WaitWheel;
    public Image DailyBonusScreen;
    public Image DailyBonusBackground;
    public float DailyBonusBackgroundOpacity = 0.8f;
    public RectTransform RechargeScreen;
}

[Serializable]
public class TopbarUI
{
    public Canvas TopBarCanvas;
    public Image TopBarPanel;
    public TextMeshProUGUI CoinsCounter;
    public TextMeshProUGUI GoldBullionsCounter;
    public TextMeshProUGUI WorldCounter;
    public TextMeshProUGUI XPCounter;
    public TextMeshProUGUI GamesCounter;
    public TextMeshProUGUI BoostersCounter;
    public TextMeshProUGUI ShieldsCounter;
    public ParticleSystem WinGoldBars;
    public ParticleSystem WinCoins;
    public ParticleSystem WinGames;
    public ParticleSystem WinBoosters;
    public ParticleSystem WinShields;
}

[Serializable]
public class LoginUI
{
    public Canvas LoginCanvas;
    public RectTransform EmailLoginScreen;
    public RectTransform UserSettingsScreen;
    public InputField EmailField;
    public InputField PasswordField;
    public TMP_InputField NameField;
    public AddImage UserImage;
    public WellcomeBonuses WellcomeBonusesDialog;
}

[Serializable]
public class BreakInUI
{
    public Canvas BreakInCanvas;
    public RectTransform BreakInScreen;
    public Timer LockDownTimer;
    public Image OpponentImage;
    public TextMeshProUGUI OpponentName;
    public Image OpponentTrophy;
    public TransferEffects SucceessScreen;
    public Image RewardScreen;
    public TextMeshProUGUI OpponentVault;
    public TextMeshProUGUI WinCodeLabel;
    public TextMeshProUGUI SecondWinLabel;
    public TextMeshProUGUI WinSum;
    public RectTransform ShieldsElements;
    public RectTransform CodeSwipedScreen;
    public RectTransform TimeOutScreen;
    public Reward RewardOptions;
    public RectTransform DegreePopup;
    public RectTransform WorldPopup;
    public TextMeshProUGUI WorldPopupText;
    public RectTransform WorldHigherPopup;
}

[Serializable]
public class ShopUI
{
    public Canvas ShopCanvas;
    public Image ShopScreen;
}

[Serializable]
public class DailyBonusUI
{
    public Canvas DailyBonusCanvas;
    public Image DailyBonusWheelScreen;
    public RectTransform DailyBonusCollectScreen;
    public TextMeshProUGUI WinSumText;
}

[Serializable]
public class ReactUI
{
    public Canvas ReactCanvas;
    public Image ReactScreen;
}

[Serializable]
public class MenuUI
{
    public Canvas MenuCanvas;
    public Image MenuPanel;
    public Toggle MusicSettings;
    public Toggle SoundEffectsSettings;
    public Toggle NotificationsSettings;
    public ProfileUI ProfileElements;
}

[Serializable]
public class ProfileUI
{
    public TextMeshProUGUI UserName;
    public Image UserImage;
    public Image Trophy;
    public TextMeshProUGUI UserDegree;
    public TextMeshProUGUI TotalHacks;
    public TextMeshProUGUI SuccessfullHacks;
    public TextMeshProUGUI FaildHacks;
    public TextMeshProUGUI SuccessfulHacksPercents;
    public TextMeshProUGUI ShortestTime;
    public TextMeshProUGUI CoinsTook;
    public TextMeshProUGUI HackStopped;
    public TextMeshProUGUI Hacked;
    public TextMeshProUGUI CoinsTaken;
}

[Serializable]
public class MessagesUI
{
    public Image OutOfGamesMessage;
    public Image OutOfGoldBarsMessage;
    public Image NotEnoughGlodBarsMessage;
    public Image OutOfBoostersMessage;
    public Image OutOfShieldsMessage;
    public Image ReminderMessage;
    public Image ErrorMessage;
}