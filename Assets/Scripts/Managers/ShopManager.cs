﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ShopManager : MonoBehaviour
{
    public enum Tabs { Gold, Games, Coins, Boosters, Shields }

    public RectTransform GoldBarsPanel;
    public RectTransform GamesPanel;
    public RectTransform CoinsPanel;
    public RectTransform BoostersPanel;
    public RectTransform ShieldsPanel;
    public AudioSource ClickSound;
    public IAPManager IAP;
    public ToggleGroup TabsGroup;

    public static ShopManager Shop;

    private Dictionary<Tabs, Toggle> _tabsToggles;

    private Dictionary<string, Dictionary<int, float>> _shopData;

    private void Awake()
    {
        if(Shop != null)
        {
            Destroy(gameObject);
            return;
        }
        Shop = this;

        DatabaseManager.Database.GotShop.AddListener(OnGotShopData);
        _tabsToggles = new Dictionary<Tabs, Toggle>
        {
            { Tabs.Gold, TabsGroup.transform.Find("GoldToggle").GetComponent<Toggle>() },
            { Tabs.Games, TabsGroup.transform.Find("GamesToggle").GetComponent<Toggle>() },
            { Tabs.Coins, TabsGroup.transform.Find("CoinsToggle").GetComponent<Toggle>() },
            { Tabs.Boosters, TabsGroup.transform.Find("BoostersToggle").GetComponent<Toggle>() },
            { Tabs.Shields, TabsGroup.transform.Find("ShieldsToggle").GetComponent<Toggle>() }
        };
    }

    private void OnGotShopData(Dictionary<string, Dictionary<int, float>> shopData)
    {
        _shopData = shopData;
    }

    public bool IsShopData()
    {
        return _shopData != null;
    }

    public void SetPrices()
    {
        Transform currentContent = null;
        DatabaseManager.UserDataKeys key = DatabaseManager.UserDataKeys.GoldBars;
        foreach (var product in _shopData.Keys)
        {
            switch (product)
            {
                case "gold_bars":
                    currentContent = GoldBarsPanel;
                    key = DatabaseManager.UserDataKeys.GoldBars;
                    break;
                case "games":
                    currentContent = GamesPanel;
                    key = DatabaseManager.UserDataKeys.Games;
                    break;
                case "coins":
                    currentContent = CoinsPanel;
                    key = DatabaseManager.UserDataKeys.Coins;
                    break;
                case "boosters":
                    currentContent = BoostersPanel;
                    key = DatabaseManager.UserDataKeys.Boosters;
                    break;
                case "shields":
                    currentContent = ShieldsPanel;
                    key = DatabaseManager.UserDataKeys.Shields;
                    break;
            }

            var keys = _shopData[product].Keys.ToList();
            keys.Sort();

            for (int i = 0; i < keys.Count; i++)
            {
                if (currentContent.childCount <= i)
                {
                    Instantiate(currentContent.GetChild(currentContent.childCount - 1), currentContent);
                }

                var sum = currentContent.GetChild(i).Find("Sum").GetComponent<TMPro.TextMeshProUGUI>();
                var sumValue = keys[i];
                sum.text = sumValue.ToString();

                var buyButton = currentContent.GetChild(i).Find("Button");
                var price = buyButton.Find("Text").GetComponent<Text>();
                var priceValue = _shopData[product][sumValue];
                if (product == "gold_bars")
                {
                    price.text = priceValue.ToString("N2");
                    //buyButton.GetComponent<Button>().onClick.AddListener(() => IAP.BuyGoldBars(sumValue));
                }
                else
                {
                    price.text = priceValue.ToString();
                    var shopButton = buyButton.GetComponent<ShopButton>();
                    if (shopButton == null)
                    {
                        shopButton = buyButton.gameObject.AddComponent<ShopButton>();
                    }
                    shopButton.Sum = sumValue;
                    shopButton.Price = (int)priceValue;
                    shopButton.Key = key;
                    shopButton.ClickSound = ClickSound;
                }
            }
        }
    }

    public void SetTab(Tabs tab)
    {
        _tabsToggles[tab].isOn = true;
    }
}
