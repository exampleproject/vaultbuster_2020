using Firebase.DynamicLinks;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinksManager : MonoBehaviour
{
    public string Subject = "Vault Buster!";
    public string Body = "Check out this awesome app!";

    public static LinksManager links;

    private void Awake()
    {
        if (links == null)
        {
            links = new LinksManager();
            return;
        }
        Destroy(gameObject);
    }

    public void StartLinks()
    {
        DynamicLinks.DynamicLinkReceived += OnDynamicLink;
    }

    void OnDynamicLink(object sender, EventArgs args)
    {
        var dynamicLinkEventArgs = args as ReceivedDynamicLinkEventArgs;
        var queryString = dynamicLinkEventArgs.ReceivedDynamicLink.Url.Query;
        var urlSender = GetQueryStringValue(dynamicLinkEventArgs.ReceivedDynamicLink.Url, "sender");
        if (!string.IsNullOrEmpty(urlSender))
        {
            FuncitionsManager.Functions.SetFriend(AppManager.App.CurrentUser.Id, urlSender);
        }

        Debug.LogFormat("Received dynamic link {0}", dynamicLinkEventArgs.ReceivedDynamicLink.Url.OriginalString);
    }

    public void ShortLink()
    {
        var components = new DynamicLinkComponents(
    // The base Link.
    new Uri("https://www.vaultbuster.com?sender=" + AppManager.App.CurrentUser.Id),
    // The dynamic link URI prefix.
    "https://vaultbuster.page.link")
        {
            //IOSParameters = new IOSParameters("com.vaultbuster.ios"),
            AndroidParameters = new AndroidParameters(Application.identifier),
        };
        // do something with: components.LongDynamicLink

        var options = new DynamicLinkOptions
        {
            PathLength = DynamicLinkPathLength.Unguessable
        };

        DynamicLinks.GetShortLinkAsync(components, options).ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                Debug.LogError("GetShortLinkAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("GetShortLinkAsync encountered an error: " + task.Exception);
                return;
            }

            // Short Link has been created.
            ShortDynamicLink link = task.Result;
            Debug.LogFormat("Generated short link {0}", link.Url);

            ShareDynamicLink(link.Url.AbsoluteUri);

            var warnings = new List<string>(link.Warnings);
            if (warnings.Count > 0)
            {
                // Debug logging for warnings generating the short link.
            }
        });
    }

    private void ShareDynamicLink(string dynamicLinkUri)
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
            AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

            intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
            intentObject.Call<AndroidJavaObject>("setType", "text/plain");
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), Subject);
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), Body + "\n" + dynamicLinkUri);

            AndroidJavaClass unityPlayerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity");

            AndroidJavaObject chooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, "Share via");
            currentActivity.Call("startActivity", chooser);
        }
    }

    private string GetQueryStringValue(Uri uri, string key)
    {
        string query = uri.Query;
        if (query.StartsWith("?"))
        {
            query = query.Substring(1);
        }

        string[] queryParams = query.Split('&');

        foreach (string param in queryParams)
        {
            string[] keyValue = param.Split('=');
            if (keyValue.Length == 2 && keyValue[0] == key)
            {
                return keyValue[1];
            }
        }

        return null;
    }

}
