﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrophiesCabinet : MonoBehaviour
{
    public string Degree = "Skilled Advanced Buster";
    public GameObject ThrophyObject;
    public GridLayoutGroup Container;

    private void OnEnable()
    {
        if (AppManager.App != null && AppManager.App.CurrentUser != null && GameProgressConfig.GameProgress != null && GameProgressConfig.GameProgress.GameProgressData != null)
        {
            Degree = GameProgressConfig.GameProgress.GetCurrentXPData().Degree;
        }

        var trophies = Trophies.TrophiesSprites.GetPrevTrophiesByDegree(Degree);

        for (var i = 0; i < trophies.Count; i++)
        {
            if (i < Container.transform.childCount)
            {
                continue;
            }
            var throphy = Instantiate(ThrophyObject, Container.transform);
            throphy.GetComponentInChildren<Image>().sprite = trophies[i];
            throphy.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = Trophies.TrophiesSprites.GetDegreeByLevel(i);
        }
        for (var i = Container.transform.childCount - 1; i >= trophies.Count; i--)
        {
            Destroy(Container.transform.GetChild(i).gameObject);
        }
    }
}
