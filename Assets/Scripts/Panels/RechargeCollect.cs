using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RechargeCollect : MonoBehaviour
{
    public int GamesSum = 5;
    public int BoostersSum = 3;
    public AudioSource CollectSound;

    public void OnCollectClick()
    {
        var topBar = UIReferences.References.TopBar.TopBarCanvas.GetComponent<TopbarManager>();
        topBar.Count(new Dictionary<DatabaseManager.UserDataKeys, int> {
            { DatabaseManager.UserDataKeys.Games, GamesSum },
            { DatabaseManager.UserDataKeys.Boosters, BoostersSum }
        });

        UIReferences.References.TopBar.WinGames.gameObject.SetActive(true);
        UIReferences.References.TopBar.WinBoosters.gameObject.SetActive(true);

        UIReferences.References.TopBar.WinGames.GetComponent<DailyCollectEffect>().NextScreen = ScreensManager.Screens.NONE;
        UIReferences.References.TopBar.WinBoosters.GetComponent<DailyCollectEffect>().NextScreen = ScreensManager.Screens.DIRECTIONS;

        CollectSound.gameObject.SetActive(true);
    }

    private void OnDisable()
    {
        CollectSound.gameObject.SetActive(false);
    }
}
