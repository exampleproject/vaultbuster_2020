﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DegreePopup : OpenDialog
{
    public Image TrophyImage;
    public TextMeshProUGUI DegreeName;

    protected override void OnEnable()
    {
        var degree = GameProgressConfig.GameProgress.GetCurrentXPData().Degree;
        TrophyImage.sprite = Trophies.TrophiesSprites.GetTrophyByDegree(degree);
        DegreeName.text = degree;
        base.OnEnable();
    }
}
