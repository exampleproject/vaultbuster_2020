﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class SpinWheel : MonoBehaviour
{
    public float AnimationTime = 4;
    public AnimationCurve AccelerationCurve;
    public AnimationCurve DecelerationCurve;
    public AudioSource SpinSound;
    public int MinSpins = 5;
    public int MaxSpins = 8;
    public float CollectPanelScaleUpTime = 0.25f;

    public RectTransform Wheel;
    public RectTransform CollectPanel;
    public DailyBonusManager Manager;
    public Button SpinButton;
    public List<Sprite> WinSprites;

    private float _stopOffsetPoint = 22.5f;

    private void OnEnable()
    {
        transform.localEulerAngles = Vector3.zero;
        Manager.ResetWheelValues();
        SpinButton.interactable = true;

        var wheelImages = Wheel.GetComponentsInChildren<Image>();
        foreach (var image in wheelImages)
        {
            image.color = new Color(image.color.r, image.color.g, 1);
        }

        var wheelTexts = Wheel.GetComponentsInChildren<TMPro.TextMeshProUGUI>();
        foreach (var text in wheelTexts)
        {
            text.color = new Color(text.color.r, text.color.g, text.color.b, 1);
        }

        var background = UIReferences.References.GameFllow.DailyBonusBackground;
        background.color = new Color(background.color.r, background.color.g, background.color.b, UIReferences.References.GameFllow.DailyBonusBackgroundOpacity);
    }

    public void OnSpin()
    {
        DatabaseManager.Database.UpdateUserDataPrameters(new Dictionary<string, object> { { DatabaseManager.Database.GetPathFromKey(DatabaseManager.UserDataKeys.NextDaily), (DateTime.Now.AddHours(8)).ToString(UIReferences.DATE_FORMAT) } });
        StartCoroutine(Spin());
    }

    IEnumerator Spin()
    {
        SpinSound.Play();

        var stopItem = Random.Range(0, 8);
        var spins = Random.Range(MinSpins, MaxSpins);
        var stopPoint = _stopOffsetPoint + (45 * stopItem) + (360 * spins);
        var halfRotation = new Vector3(0, 0, stopPoint * 0.5f);
        var time = AnimationTime * 0.5f;

        var winSum = Manager.GetWinSum(stopItem);

        CollectPanel.Find("WinText").GetComponent<TMPro.TextMeshProUGUI>().text = "X" + winSum;
        CollectPanel.Find("WinImage").GetComponent<Image>().sprite = WinSprites[stopItem];
        var collectDailyBonus = CollectPanel.GetComponent<CollectDailyBonus>();

        for (float timer = 0; timer < time; timer += Time.deltaTime)
        {
            var curvePercent = AccelerationCurve.Evaluate(timer / time);
            transform.eulerAngles = Vector3.Lerp(Vector3.zero, halfRotation, curvePercent);
            yield return null;
        }
        transform.eulerAngles = halfRotation;

        var stopRotation = new Vector3(0, 0, stopPoint);
        for (float timer = 0; timer < time; timer += Time.deltaTime)
        {
            var curvePercent = DecelerationCurve.Evaluate(timer / time);
            transform.eulerAngles = Vector3.Lerp(halfRotation, stopRotation, curvePercent);
            yield return null;
        }
        transform.eulerAngles = stopRotation;

        var topBar = UIReferences.References.TopBar.TopBarCanvas.GetComponent<TopbarManager>();
        switch (stopItem)
        {
            case 0:
                topBar.Count(new Dictionary<DatabaseManager.UserDataKeys, int> { { DatabaseManager.UserDataKeys.GoldBars, winSum } });
                collectDailyBonus.WinObject = "gold_bars";
                break;
            case 1:
                topBar.Count(new Dictionary<DatabaseManager.UserDataKeys, int> { { DatabaseManager.UserDataKeys.Coins, winSum } });
                collectDailyBonus.WinObject = "coins";
                break;
            case 2:
                topBar.Count(new Dictionary<DatabaseManager.UserDataKeys, int> { { DatabaseManager.UserDataKeys.Shields, winSum } });
                collectDailyBonus.WinObject = "shields";
                break;
            case 3:
                topBar.Count(new Dictionary<DatabaseManager.UserDataKeys, int> { { DatabaseManager.UserDataKeys.Boosters, winSum } });
                collectDailyBonus.WinObject = "boosters";
                break;
            case 4:
                topBar.Count(new Dictionary<DatabaseManager.UserDataKeys, int> { { DatabaseManager.UserDataKeys.GoldBars, winSum } });
                collectDailyBonus.WinObject = "gold_bars";
                break;
            case 5:
                topBar.Count(new Dictionary<DatabaseManager.UserDataKeys, int> { { DatabaseManager.UserDataKeys.Coins, winSum } });
                collectDailyBonus.WinObject = "coins";
                break;
            case 6:
                topBar.Count(new Dictionary<DatabaseManager.UserDataKeys, int> { { DatabaseManager.UserDataKeys.Boosters, winSum } });
                collectDailyBonus.WinObject = "boosters";
                break;
            case 7:
                topBar.Count(new Dictionary<DatabaseManager.UserDataKeys, int> { { DatabaseManager.UserDataKeys.Games, winSum } });
                collectDailyBonus.WinObject = "games";
                break;
        }

        yield return new WaitForSeconds(1);

        UIReferences.References.ActiveScreen(ScreensManager.Screens.DAILY_BONUS_COLLECT, ScreensManager.Requires.NONE);
    }
}
