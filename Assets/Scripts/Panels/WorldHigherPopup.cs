﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class WorldHigherPopup : OpenDialog
{
    public string[] Directions;
    public GameObject DirectionCell;
    public GridLayoutGroup DirectionsHolder;

    private List<Sprite> _directionsImages;

    private void Awake()
    {
        _directionsImages = Resources.LoadAll<Sprite>("Sprites/DirectionPopups").ToList();
    }

    protected override void OnEnable()
    {
        foreach(Transform child in DirectionsHolder.transform)
        {
            Destroy(child.gameObject);
        }

        if (GameProgressConfig.GameProgress != null && GameProgressConfig.GameProgress.GameProgressData != null)
        {
            var configRaw = GameProgressConfig.GameProgress.GetCurrentXPData();
            Directions = configRaw.Directions.Split();
        }
        Debug.Log(Directions[Directions.Length - 1]);
        switch (Directions[Directions.Length - 1])
        {
            case "left-right":
                AddDirectionCell(CodeShapes.Right);
                AddDirectionCell(CodeShapes.Left);
                break;
            case "up-down":
                AddDirectionCell(CodeShapes.Up);
                AddDirectionCell(CodeShapes.Down);
                break;
            case "diagonals":
                AddDirectionCell(CodeShapes.RightUp);
                AddDirectionCell(CodeShapes.RightDown);
                AddDirectionCell(CodeShapes.LeftUp);
                AddDirectionCell(CodeShapes.LeftDown);
                break;
            case "curved-vertical":
                AddDirectionCell(CodeShapes.UpRightUp);
                AddDirectionCell(CodeShapes.UpLeftUp);
                AddDirectionCell(CodeShapes.DownRightDown);
                AddDirectionCell(CodeShapes.DownLeftDown);
                break;
            case "curved-horizontal":
                AddDirectionCell(CodeShapes.RightUpRight);
                AddDirectionCell(CodeShapes.RightDownRight);
                AddDirectionCell(CodeShapes.LeftUpLeft);
                AddDirectionCell(CodeShapes.LeftDownLeft);
                break;
            case "zigzag-left":
                AddDirectionCell(CodeShapes.ZigzagLeftUp);
                AddDirectionCell(CodeShapes.ZigzagLeftDown);
                AddDirectionCell(CodeShapes.ZigzagUpLeft);
                AddDirectionCell(CodeShapes.ZigzagDownLeft);
                break;
            case "zigzag-right":
                AddDirectionCell(CodeShapes.ZigzagRightUp);
                AddDirectionCell(CodeShapes.ZigzagRightDown);
                AddDirectionCell(CodeShapes.ZigzagUpRight);
                AddDirectionCell(CodeShapes.ZigzagDownRight);
                break;
        }
        base.OnEnable();
    }

    private void AddDirectionCell(CodeShapes shape)
    {
        var cell = Instantiate(DirectionCell, DirectionsHolder.transform);
        cell.GetComponent<Image>().sprite = _directionsImages.Single(d => d.name == shape.ToString());
    }
}
