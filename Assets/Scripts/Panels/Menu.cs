using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour
{
    public Animator MenuAnimator;
    public GameObject GeneranlSoundEffects;
    public List<AudioSource> SoundEffectsSources;
    public SkillLevelButton PlayButton;
    public string HelpUrl = "https://youtu.be/mEirhkT5ij4";
    public string PrivacyPolicyUrl = "http://playtiz.com/privacy-policy/";

    public void OnMenuButtonClick(int menuState)
    {
        var state = MenuAnimator.GetInteger("MenuState");
        if (menuState == 1 && state == 0)
        {
            SetProfileElements();
        }
        if (menuState == -1)
        {
            if (state == 1)
            {
                MenuAnimator.SetInteger("MenuState", 0);
            }
            else
            {
                MenuAnimator.SetInteger("MenuState", 1);
            }
        }
        else
        {
            MenuAnimator.SetInteger("MenuState", menuState);
        }
    }

    public void OnPlayButtonClick()
    {
        if (AppManager.App.OpponentPlayer != null && UIReferences.References.BreakIn.LockDownTimer.Seconds > 0)
        {
            UIReferences.References.ActiveScreen(ScreensManager.Screens.BREAKIN, ScreensManager.Requires.NONE);
        }
        else
        {
            if (AppManager.App.CheckEnoughAmount(DatabaseManager.UserDataKeys.Games))
            {
                GameProgressConfig.GameProgress.SetChoosenIndex(AppManager.App.CurrentUser.XP);
                UIReferences.References.ActiveScreen(ScreensManager.Screens.CHOOSE_PLAYER, ScreensManager.Requires.NONE);
            }
            else
            {
                UIReferences.References.ShowMessage(UIReferences.MessagesTypes.OutOfGamesMessage);
            }
        }
    }

    public void MuteMusic(bool isNotMute)
    {
        UIReferences.References.BreakIn.BreakInScreen.GetComponent<AudioSource>().mute = !isNotMute;
        foreach (var audioSource in UIReferences.References.BreakIn.SucceessScreen.GetComponentsInChildren<AudioSource>(true))
        {
            audioSource.mute = !isNotMute;
        }
        foreach (var audioSource in UIReferences.References.BreakIn.RewardScreen.GetComponentsInChildren<AudioSource>(true))
        {
            audioSource.mute = !isNotMute;
        }
        GameConfigManager.GameConfig.IsMusic = isNotMute;
    }

    public void MuteSoundEffects(bool isNotMute)
    {
        foreach (var audioSource in GeneranlSoundEffects.GetComponentsInChildren<AudioSource>())
        {
            audioSource.mute = !isNotMute;
        }
        foreach (var audioSource in SoundEffectsSources)
        {
            audioSource.mute = !isNotMute;
        }
        GameConfigManager.GameConfig.IsSoundEffects = isNotMute;
    }

    public void RateUs()
    {
#if UNITY_ANDROID
        Application.OpenURL("market://details?id=com.playtiz.androidVaultBuster");
#endif
    }

    private void SetProfileElements()
    {
        var user = AppManager.App.CurrentUser;
        if (user == null)
        {
            return;
        }
        var valueFormat = "#,#";
        var profileElements = UIReferences.References.Menu.ProfileElements;
        profileElements.UserName.text = user.Name;
        var degree = GameProgressConfig.GameProgress.GetCurrentXPData().Degree;
        profileElements.Trophy.sprite = Trophies.TrophiesSprites.GetTrophyByDegree(degree);
        profileElements.UserDegree.text = degree;
        profileElements.TotalHacks.text = GetValueWithFormat(user.UserGamesProgress.TotalGames, valueFormat);
        profileElements.SuccessfullHacks.text = GetValueWithFormat(user.UserGamesProgress.TotalWins, valueFormat);
        profileElements.FaildHacks.text = GetValueWithFormat(user.UserGamesProgress.TotalGames - user.UserGamesProgress.TotalWins, valueFormat);
        profileElements.SuccessfulHacksPercents.text = (((float)user.UserGamesProgress.TotalWins / user.UserGamesProgress.TotalGames) * 100).ToString("N0") + "%";
        profileElements.ShortestTime.text = user.UserGamesProgress.ShortestHackingTime == 0 ? "--" : user.UserGamesProgress.ShortestHackingTime + " SEC";
        profileElements.CoinsTook.text = GetValueWithFormat(user.UserGamesProgress.CoinsTook, valueFormat);
        profileElements.HackStopped.text = GetValueWithFormat(user.UserGamesProgress.HackSttoped, valueFormat); ;
        profileElements.Hacked.text = GetValueWithFormat(user.UserGamesProgress.HackedTimes, valueFormat); ;
        profileElements.CoinsTaken.text = GetValueWithFormat(user.UserGamesProgress.CoinsTaken, valueFormat); ;
    }

    private string GetValueWithFormat(int value, string format)
    {
        return value.ToString(value == 0 ? "N0" : format);
    }

    public void OnHelpButtonClick()
    {
        Application.OpenURL(HelpUrl);
    }
    public void OnPrivacyPolicyButtonClick()
    {
        Application.OpenURL(PrivacyPolicyUrl);
    }
}
