﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Random = UnityEngine.Random;

public class BreakIn : AnimationCode
{
    [Header("Break In Parameters")]
    public int CodeLength = 4;
    public int CodeRounds = 2;
    public int BoardColumns = 8;
    public int BoardRows = 9;
    public float GridSpacing = 0;
    public int DarkenDigitsOverNumber = 10;
    public int BlinkTimes = 3;
    public float BlinkTime = 0.5f;
    public Color BaseColor;
    public Color DarkenColor = new Color32(49, 49, 49, 153);
    public Color SuccessColor = Color.blue;
    public Color FaildColor = Color.red;
    public Timer RandomizeTimer;
    public Image RandomizeText;
    public ShapeArrow ShapeArrowObject;
    public DirectionArrow DirectionArrowObject;
    public Image SuccessImage;
    public Image FaildImage;
    public TextMeshProUGUI CodeLabel;
    public TextMeshProUGUI CodeText;
    public RectTransform BoardHolder;
    public CellButton CellButton;
    public BoostersCounter BottomBoostersCounter;

    public UnityEventInt FinishBoardEvent;

    private List<int> _vaultCode = new List<int>();
    private Dictionary<int, CellButton> _allCells = new Dictionary<int, CellButton>();
    private List<CellButton> _selectedCells = new List<CellButton>();
    private int _successCounter;
    private BoardBuilder _boardBuilder;
    private Coroutine _denideFeedback;

    private List<CodeShapes> _shapes = new List<CodeShapes>();
    private int _fakeCodesNumber = 3;
    private int _codeValuesDigits = 10;
    private int _roundTime = 30;
    private int _totalGameTime;
    private int _hackTime;
    private TextMeshProUGUI _randomizeCounter;
    private string[] _roundNubers = new string[] { "FIRST", "SECOND" };
    private Transform _boosters;
    private string _codeLabel;
    private string _opponentName;
    private bool _isReset = true;

    private void Awake()
    {
        _boardBuilder = new BoardBuilder(BoardColumns, BoardRows);
        _randomizeCounter = RandomizeTimer.GetComponentInChildren<TextMeshProUGUI>();
        _boosters = transform.Find("Boosters");
        _codeLabel = CodeLabel.text;
        MessagingManager.Messaging.CodeSwiped.AddListener(OnCodeSwiped);
    }

    private void OnCodeSwiped()
    {
        _opponentName = string.Empty;
    }

    private void OnEnable()
    {
        if (!string.IsNullOrEmpty(_opponentName) && AppManager.App.OpponentPlayer.Name == _opponentName)
        {
            ContinueGame();
            return;
        }
        else
        {
            ResetParameters();
        }

        if (_isReset)
        {
            _isReset = false;
            if (AppManager.App.OpponentPlayer.AvatarIndex == -1)
            {
                if (AppManager.App.OpponentPlayer.PlayerImage == null)
                {
                    StorageManager.Storage.SpriteDownloaded.AddListener(OnSpriteDownloaded);
                    StorageManager.Storage.DownloadPlayerImage(AppManager.App.OpponentPlayer.Uid, AppManager.App.OpponentPlayer.ImageUrl);
                }
                else
                {
                    UIReferences.References.BreakIn.OpponentImage.sprite = AppManager.App.OpponentPlayer.PlayerImage;
                }
            }
            _opponentName = AppManager.App.OpponentPlayer.Name;
            UIReferences.References.BreakIn.OpponentName.text = _opponentName;

            if (GameProgressConfig.GameProgress != null && GameProgressConfig.GameProgress.GameProgressData != null)
            {
                var configRaw = GameProgressConfig.GameProgress.GetChoosenXPData();

                _shapes.Clear();

                foreach (var direction in configRaw.Directions.Split())
                {
                    switch (direction)
                    {
                        case "left-right":
                            _shapes.Add(CodeShapes.Right);
                            _shapes.Add(CodeShapes.Left);
                            break;
                        case "up-down":
                            _shapes.Add(CodeShapes.Up);
                            _shapes.Add(CodeShapes.Down);
                            break;
                        case "diagonals":
                            _shapes.Add(CodeShapes.RightUp);
                            _shapes.Add(CodeShapes.RightDown);
                            _shapes.Add(CodeShapes.LeftUp);
                            _shapes.Add(CodeShapes.LeftDown);
                            break;
                        case "curved-up-down":
                            _shapes.Add(CodeShapes.UpRightUp);
                            _shapes.Add(CodeShapes.LeftUpLeft);
                            _shapes.Add(CodeShapes.DownRightDown);
                            _shapes.Add(CodeShapes.DownLeftDown);
                            break;
                        case "curved-left-right":
                            _shapes.Add(CodeShapes.RightUpRight);
                            _shapes.Add(CodeShapes.RightDownRight);
                            _shapes.Add(CodeShapes.LeftUpLeft);
                            _shapes.Add(CodeShapes.LeftDownLeft);
                            break;
                        case "zigzag-left":
                            _shapes.Add(CodeShapes.ZigzagLeftUp);
                            _shapes.Add(CodeShapes.ZigzagLeftDown);
                            _shapes.Add(CodeShapes.ZigzagUpLeft);
                            _shapes.Add(CodeShapes.ZigzagDownLeft);
                            break;
                        case "zigzag-right":
                            _shapes.Add(CodeShapes.ZigzagRightUp);
                            _shapes.Add(CodeShapes.ZigzagRightDown);
                            _shapes.Add(CodeShapes.ZigzagUpRight);
                            _shapes.Add(CodeShapes.ZigzagDownRight);
                            break;
                    }
                }

                _fakeCodesNumber = configRaw.Variations;
                _codeValuesDigits = configRaw.CodeDigits;
                _roundTime = configRaw.RefreshSeconds;
                _totalGameTime = configRaw.TotalGamesTime;
            }
            else
            {
                _shapes = Enum.GetValues(typeof(CodeShapes)).OfType<CodeShapes>().ToList();
                _totalGameTime = _roundTime * 3;
            }

            StartCoroutine(OrderBoard());
        }
    }

    private void ContinueGame()
    {
        if (!UIReferences.References.BreakIn.LockDownTimer.IsPause)
        {
            var totalTime = UIReferences.References.BreakIn.LockDownTimer.Seconds;
            var currentTime = UIReferences.References.BreakIn.LockDownTimer.CurrentTime;
            UIReferences.References.BreakIn.LockDownTimer.StopTimer();
            RandomizeTimer.StopTimer();
            var fullRandomizeSeconds = totalTime - (totalTime % RandomizeTimer.Seconds);
            var randomizeRemain = fullRandomizeSeconds - (totalTime - currentTime);
            var randomizeCurrentTime = randomizeRemain % RandomizeTimer.Seconds;
            if (randomizeCurrentTime == 0 && currentTime >= RandomizeTimer.Seconds)
            {
                randomizeCurrentTime = RandomizeTimer.Seconds;
            }
            RandomizeTimer.StartTimer(randomizeCurrentTime);
            UIReferences.References.BreakIn.LockDownTimer.StartTimer(currentTime);
        }
    }

    private IEnumerator DisplayCode()
    {
        CodeText.text = string.Empty;
        for (int i = 0; i < CodeLength; i++)
        {
            if (i > 0)
            {
                CodeText.text += "<size=0.6em><voffset=0.2em>></voffset></size>";
            }
            CodeText.text += _vaultCode[i].ToString("D2");
            yield return new WaitForSeconds(0.25f);
        }
    }

    private IEnumerator OrderBoard()
    {
        CodeLabel.text = string.Format(_codeLabel, _roundNubers[_successCounter], _successCounter + 1, CodeRounds);
        PlayAnimation("BlinkCodeLabel", BlinkCodeLabel);

        var boardNumbersDict = _boardBuilder.CreateBoard(CodeLength, true, _shapes, _fakeCodesNumber, _codeValuesDigits);
        _vaultCode = _boardBuilder.Code;

        var cellWidth = (BoardHolder.rect.width - (GridSpacing * (BoardColumns - 1))) / BoardColumns;
        var cellHeight = (BoardHolder.rect.height - (GridSpacing * (BoardRows - 1))) / BoardRows;

        GridLayoutGroup grid;
        grid = BoardHolder.gameObject.GetComponent<GridLayoutGroup>();
        if (grid == null)
        {
            grid = BoardHolder.gameObject.AddComponent<GridLayoutGroup>();
        }
        grid.cellSize = new Vector2(cellWidth, cellHeight);
        grid.spacing = new Vector2(GridSpacing, GridSpacing);

        _allCells.Clear();

        for (int i = 0; i < BoardColumns * BoardRows; i++)
        {
            var cell = Instantiate(CellButton, grid.transform);
            cell.Value = boardNumbersDict[i];
            _allCells[i] = cell;
            cell.IsSelectable = true;
            cell.Selected.AddListener(OnSelected);
            cell.Deselected.AddListener(OnDeselected);
        }

        yield return DisplayCode();
        CodeText.text = "";
        yield return new WaitForSeconds(1f);
        yield return DisplayCode();

        RandomizeTimer.Seconds = _roundTime;
        UIReferences.References.BreakIn.LockDownTimer.Seconds = _totalGameTime;
        if (_totalGameTime >= _roundTime)
        {
            if (!RandomizeText.gameObject.activeInHierarchy)
            {
                RandomizeText.gameObject.SetActive(true);
            }
            if (!_randomizeCounter.gameObject.activeInHierarchy)
            {
                _randomizeCounter.gameObject.SetActive(true);
            }
            RandomizeTimer.StartTimer();
        }
        else
        {
            if (RandomizeText.gameObject.activeInHierarchy)
            {
                RandomizeText.gameObject.SetActive(false);
            }
            if (_randomizeCounter.gameObject.activeInHierarchy)
            {
                _randomizeCounter.gameObject.SetActive(false);
            }
        }
        UIReferences.References.BreakIn.LockDownTimer.StartTimer();
    }

    IEnumerable ReOrderBoard()
    {
        yield return null;
        var boardNumbersDict = _boardBuilder.CreateBoard(CodeLength, false, _shapes, _fakeCodesNumber, _codeValuesDigits);
        for (int i = 0; i < BoardColumns * BoardRows; i++)
        {
            PlayAnimation("FlipDigits_1", FlipDigits, _allCells[i].gameObject);
        }
        yield return new WaitForSeconds(0.25f);
        for (int i = 0; i < BoardColumns * BoardRows; i++)
        {
            _allCells[i].GetComponentInChildren<Text>().color = BaseColor;
            _allCells[i].Value = boardNumbersDict[i];
            _allCells[i].IsSelectable = true;
        }
        for (int i = 0; i < BoardColumns * BoardRows; i++)
        {
            PlayAnimation("FlipDigits_2", FlipDigits, _allCells[i].gameObject);
        }
        yield return new WaitForSeconds(0.25f);
    }

    private void CodeScale(GameObject animatedObject, float percent)
    {
        animatedObject.GetComponent<RectTransform>().localScale = Vector3.Lerp(Vector3.one, Vector3.one * 1.15f, percent);
    }

    private void FlipDigits(GameObject animatedObject, float percent)
    {
        animatedObject.GetComponent<RectTransform>().localScale = Vector3.Lerp(Vector3.one, new Vector3(1, 0, 1), percent);
    }

    private void OnSelected(CellButton cell)
    {
        if (_selectedCells.Count < CodeLength)
        {
            _selectedCells.Add(cell);
        }

        if (_selectedCells.Count == CodeLength)
        {
            var isSuccess = true;
            for (int i = 0; i < CodeLength; i++)
            {
                var cellKey = _allCells.FirstOrDefault(c => c.Value == _selectedCells[i]).Key;
                if (!_boardBuilder.CodePositions.Contains(cellKey))
                {
                    isSuccess = false;
                    break;
                }
            }
            if (isSuccess)
            {
                StartCoroutine(SuccessFeedback());
            }
            else
            {
                if (_denideFeedback == null)
                {
                    _denideFeedback = StartCoroutine(DeniedFeedback());
                }
            }
        }
    }

    IEnumerator DeniedFeedback()
    {
        foreach (var cellButton in _allCells.Values)
        {
            cellButton.IsSelectable = false;
        }

        yield return new WaitForSeconds(1);

        foreach (var selectedCell in _selectedCells)
        {
            var label = selectedCell.GetComponentInChildren<Text>();
            label.color = FaildColor;
        }

        FaildImage.gameObject.SetActive(true);

        yield return new WaitForSeconds(1);

        ResetSelected();
    }

    private void ResetSelected()
    {
        if (_denideFeedback != null)
        {
            StopCoroutine(_denideFeedback);
            _denideFeedback = null;
        }

        foreach (var selectedCell in _selectedCells)
        {
            var label = selectedCell.GetComponentInChildren<Text>();
            label.color = BaseColor;
        }

        FaildImage.gameObject.SetActive(false);

        for (int i = 0; i < _selectedCells.Count; i++)
        {
            _selectedCells[i].IsOn = false;
        }

        _selectedCells.Clear();

        foreach (var cellButton in _allCells.Values)
        {
            cellButton.IsSelectable = true;
        }
    }

    private IEnumerator SuccessFeedback()
    {
        _successCounter++;

        if (_successCounter == CodeRounds)
        {
            FuncitionsManager.Functions.TransferCoins();
        }

        foreach (var cellButton in _allCells.Values)
        {
            cellButton.IsSelectable = false;
        }

        foreach (var selectedCell in _selectedCells)
        {
            var label = selectedCell.GetComponentInChildren<Text>();
            label.color = SuccessColor;
        }

        RandomizeTimer.PauseTimer();
        UIReferences.References.BreakIn.LockDownTimer.PauseTimer();
        SuccessImage.gameObject.SetActive(true);

        if (_successCounter < CodeRounds)
        {
            yield return new WaitForSeconds(2.873f);
        }
        else
        {
            yield return new WaitForSeconds(1f);
        }

        if (_successCounter == CodeRounds)
        {
            float timer = 0;
            while (!AppManager.App.IsGotReward && timer < 20)
            {
                timer += Time.deltaTime;
                yield return null;
            }
            if (timer >= 20)
            {
                UIReferences.References.ShowMessage(UIReferences.MessagesTypes.Error);
                yield break;
            }
            UIReferences.References.TopBar.CoinsCounter.text = AppManager.App.CurrentUser.Vault.Coins.ToString("N0");
            var sound = GetComponent<AudioSource>();
            for (float percent = 1; percent > 0; percent -= Time.deltaTime)
            {
                sound.volume = percent;
                yield return null;
            }
            sound.Stop();
            sound.volume = 1;
        }

        foreach (var selectedCell in _selectedCells)
        {
            selectedCell.GetComponentInChildren<Text>().color = BaseColor;
        }

        SuccessImage.gameObject.SetActive(false);

        if (_successCounter == 1)
        {
            UIReferences.References.BreakIn.WinCodeLabel.text = CodeText.text;
        }
        else
        {
            UIReferences.References.BreakIn.WinCodeLabel.text += " " + CodeText.text;
        }

        _hackTime += UIReferences.References.BreakIn.LockDownTimer.Seconds - UIReferences.References.BreakIn.LockDownTimer.CurrentTime;

        ResetParameters(true);

        if (_successCounter == CodeRounds)
        {
            AppManager.App.CurrentUser.UserGamesProgress.TotalGames += 1;
            AppManager.App.CurrentUser.UserGamesProgress.TotalWins += 1;
            AppManager.App.CurrentUser.UserGamesProgress.CurrentGames += 1;
            AppManager.App.CurrentUser.UserGamesProgress.CurrentWins += 1;
            if (AppManager.App.CurrentUser.UserGamesProgress.ShortestHackingTime > _hackTime || AppManager.App.CurrentUser.UserGamesProgress.ShortestHackingTime == 0)
            {
                DatabaseManager.Database.UpdateUserDataPrameters(new Dictionary<string, object> { { DatabaseManager.Database.GetPathFromKey(DatabaseManager.UserDataKeys.ShortestHackingTime), _hackTime } });
            }
            _hackTime = 0;

            var parametersToUpdate = new Dictionary<string, object>
            {
                { DatabaseManager.Database.GetPathFromKey(DatabaseManager.UserDataKeys.TotalGames), AppManager.App.CurrentUser.UserGamesProgress.TotalGames },
                { DatabaseManager.Database.GetPathFromKey(DatabaseManager.UserDataKeys.TotalWins), AppManager.App.CurrentUser.UserGamesProgress.TotalWins },
                { DatabaseManager.Database.GetPathFromKey(DatabaseManager.UserDataKeys.CurrentGames), AppManager.App.CurrentUser.UserGamesProgress.CurrentGames },
                { DatabaseManager.Database.GetPathFromKey(DatabaseManager.UserDataKeys.CurrentWins), AppManager.App.CurrentUser.UserGamesProgress.CurrentWins }
            };
            if (GameProgressConfig.GameProgress.GetChoosenXPData().UpTo == GameProgressConfig.GameProgress.GetCurrentXPData().UpTo && AppManager.App.CurrentUser.UserGamesProgress.CurrentGames >= GameProgressConfig.GameProgress.GetCurrentXPData().MinTotalGames && AppManager.App.CurrentUser.UserGamesProgress.CurrentWins >= GameProgressConfig.GameProgress.GetCurrentXPData().SuccessiveWin)
            {
                parametersToUpdate[DatabaseManager.Database.GetPathFromKey(DatabaseManager.UserDataKeys.CurrentGames)] = 0;
                parametersToUpdate[DatabaseManager.Database.GetPathFromKey(DatabaseManager.UserDataKeys.CurrentWins)] = 0;
                AppManager.App.CurrentUser.XP += 1;
                parametersToUpdate.Add(DatabaseManager.Database.GetPathFromKey(DatabaseManager.UserDataKeys.XP), AppManager.App.CurrentUser.XP);
                UIReferences.References.BreakIn.RewardOptions.IsNewXP = true;
            }
            else
            {
                UIReferences.References.BreakIn.RewardOptions.IsNewXP = false;
            }
            DatabaseManager.Database.UpdateUserDataPrameters(parametersToUpdate);

            _successCounter = 0;
            UIReferences.References.ActiveScreen(ScreensManager.Screens.TRANSFER_EFFECT, ScreensManager.Requires.NONE);
        }
        else
        {
            _boosters.gameObject.SetActive(false);
            _boosters.gameObject.SetActive(true);
            StartCoroutine(OrderBoard());
        }
    }

    private void OnDeselected(CellButton cell)
    {
        if (_selectedCells.Contains(cell))
        {
            _selectedCells.Remove(cell);
        }
    }

    public void OnRndomizeTimeIsOut()
    {
        AppManager.App.GetComponent<MonoBehaviour>().StartCoroutine(EndFaildTry());
    }

    IEnumerator EndFaildTry()
    {
        UIReferences.References.BreakIn.LockDownTimer.PauseTimer();

        yield return null;
        ResetSelected();
        var boardNumbersDict = _boardBuilder.CreateBoard(CodeLength, false, _shapes, _fakeCodesNumber, _codeValuesDigits);
        var cellsIndexes = Enumerable.Range(0, BoardColumns * BoardRows).ToList();
        foreach (var cellBtn in _allCells.Values)
        {
            cellBtn.IsSelectable = false;
        }
        for (int i = 0; i < BoardColumns * BoardRows; i++)
        {
            var cellIndex = cellsIndexes[Random.Range(0, cellsIndexes.Count)];
            cellsIndexes.Remove(cellIndex);
            PlayAnimation("FlipGigits_1", FlipDigits, _allCells[cellIndex].gameObject);
            _allCells[cellIndex].GetComponentInChildren<Text>().color = BaseColor;
            _allCells[cellIndex].Value = boardNumbersDict[cellIndex];
            PlayAnimation("FlipGigits_2", FlipDigits, _allCells[cellIndex].gameObject);
            yield return new WaitForSeconds(0.05f);
        }
        yield return new WaitForSeconds(1.5f);
        foreach (var cellBtn in _allCells.Values)
        {
            cellBtn.IsSelectable = true;
        }
        if (UIReferences.References.BreakIn.LockDownTimer.CurrentTime >= _roundTime)
        {
            if (!RandomizeText.gameObject.activeInHierarchy)
            {
                RandomizeText.gameObject.SetActive(true);
            }
            if (!_randomizeCounter.gameObject.activeInHierarchy)
            {
                _randomizeCounter.gameObject.SetActive(true);
            }
            RandomizeTimer.StartTimer();
        }
        else
        {
            if (RandomizeText.gameObject.activeInHierarchy)
            {
                RandomizeText.gameObject.SetActive(false);
            }
            if (_randomizeCounter.gameObject.activeInHierarchy)
            {
                _randomizeCounter.gameObject.SetActive(false);
            }
        }
        UIReferences.References.BreakIn.LockDownTimer.ContinueTimer();
    }

    public void OnLockDownTimeIsOut()
    {
        DatabaseManager.Database.UpdateUserDataPrameters(new Dictionary<string, object>
        {
            { DatabaseManager.Database.GetPathFromKey(DatabaseManager.UserDataKeys.TotalGames), AppManager.App.CurrentUser.UserGamesProgress.TotalGames + 1 },
            { DatabaseManager.Database.GetPathFromKey(DatabaseManager.UserDataKeys.CurrentGames), AppManager.App.CurrentUser.UserGamesProgress.CurrentGames + 1 }
        });
        ResetParameters(true);
        UIReferences.References.ActiveScreen(ScreensManager.Screens.TIME_OUT, ScreensManager.Requires.NONE);
    }

    private void ResetParameters(bool isResetOpponent = false)
    {
        _isReset = true;
        SuccessImage.gameObject.SetActive(false);
        FaildImage.gameObject.SetActive(false);
        _vaultCode.Clear();
        _selectedCells.Clear();
        CodeText.text = string.Empty;

        foreach (Transform cell in BoardHolder.transform)
        {
            Destroy(cell.gameObject);
        }

        RandomizeTimer.ResetTimer();
        UIReferences.References.BreakIn.LockDownTimer.ResetTimer();

        if (isResetOpponent)
        {
            _opponentName = string.Empty;
        }
    }

    public void OnAddTimeBooster()
    {
        if (!BoosterPayment(1))
        {
            return;
        }
        RandomizeTimer.AddTime(10);
        UIReferences.References.BreakIn.LockDownTimer.AddTime(10);
    }

    public void OnDarkenThirdBoardBooster()
    {
        if (!BoosterPayment(1))
        {
            return;
        }
        foreach (var pos in _boardBuilder.GetThirdWithoutCode())
        {
            _allCells[pos].IsSelectable = false;
            if (_selectedCells.Contains(_allCells[pos]))
            {
                _allCells[pos].IsOn = false;
                _selectedCells.Remove(_allCells[pos]);
            }
            var cellValue = BoardHolder.GetChild(pos).GetComponentInChildren<Text>();
            cellValue.color = DarkenColor;
        }
    }

    public void OnShapeBooster()
    {
        if (!BoosterPayment(1))
        {
            return;
        }
        ShapeArrowObject.Shape = _boardBuilder.CodeShape.Value;
        ShapeArrowObject.gameObject.SetActive(true);
    }

    public void OnArrowDirectionBooster()
    {
        if (!BoosterPayment(1))
        {
            return;
        }
        DirectionArrowObject.Shape = _boardBuilder.CodeShape.Value;
        DirectionArrowObject.gameObject.SetActive(true);
    }

    public void OnDarkenDigitsOverBooster()
    {
        if (!BoosterPayment(1))
        {
            return;
        }
        foreach (var pos in _boardBuilder.GetNoneCodeNumber(DarkenDigitsOverNumber))
        {
            var cellValue = BoardHolder.GetChild(pos).GetComponentInChildren<Text>();
            cellValue.color = DarkenColor;
        }
    }

    public void OnFirstDigitFlashBooster()
    {
        if (!BoosterPayment(1))
        {
            return;
        }
        foreach (var pos in _boardBuilder.GetFirstCodeDigits())
        {
            if (_allCells[pos].IsSelectable)
            {
                var cellValue = BoardHolder.GetChild(pos).GetComponentInChildren<Text>();
                StartCoroutine(FlashDigit(cellValue));
            }
        }
    }

    private bool BoosterPayment(int payment)
    {
        if (AppManager.App.CheckEnoughAmount(DatabaseManager.UserDataKeys.Boosters))
        {
            AppManager.App.CurrentUser.Vault.Boosters -= payment;
            DatabaseManager.Database.UpdateUserDataPrameters(new Dictionary<string, object> { { DatabaseManager.Database.GetPathFromKey(DatabaseManager.UserDataKeys.Boosters), AppManager.App.CurrentUser.Vault.Boosters } });
            BottomBoostersCounter.UpdateCounter();
            return true;
        }
        UIReferences.References.ShowMessage(UIReferences.MessagesTypes.OutOfBoostersMessage);
        return false;

    }

    IEnumerator FlashDigit(Text cell)
    {
        for (var i = 0; i < BlinkTimes; i++)
        {
            for (float timer = 0; timer < BlinkTime * 0.5; timer += Time.deltaTime)
            {
                cell.color = new Color(cell.color.r, cell.color.g, cell.color.b, 1 - (timer / (BlinkTime * 0.5f)));
                yield return null;
            }

            cell.color = new Color(cell.color.r, cell.color.g, cell.color.b, 0);

            for (float timer = 0; timer < BlinkTime * 0.5; timer += Time.deltaTime)
            {
                cell.color = new Color(cell.color.r, cell.color.g, cell.color.b, timer / (BlinkTime * 0.5f));
                yield return null;
            }

            cell.color = new Color(cell.color.r, cell.color.g, cell.color.b, 1);
        }
    }

    private void OnSpriteDownloaded(string id, Sprite sprite)
    {
        if (id == AppManager.App.OpponentPlayer.Uid)
        {
            UIReferences.References.BreakIn.OpponentImage.sprite = sprite;
        }
        StorageManager.Storage.SpriteDownloaded.RemoveListener(OnSpriteDownloaded);
    }

    private void BlinkCodeLabel(GameObject o, float percent)
    {
        CodeLabel.color = new Color(CodeLabel.color.r, CodeLabel.color.g, CodeLabel.color.b, percent);
    }

    private void OnDisable()
    {
        if (UIReferences.References.BreakIn.LockDownTimer.CurrentTime > 0)
        {
            AppManager.App.IsAttacking = true;
        }
    }
}