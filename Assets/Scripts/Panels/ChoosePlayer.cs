﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Random = UnityEngine.Random;
using UnityEngine.EventSystems;

public class ChoosePlayer : MonoBehaviour
{
    public int BotsPercents = 75;
    public ToggleGroup Tabs;
    public TextMeshProUGUI PlayerName;
    public Image PlayerImage;
    public Image PlayerTrophy;
    public TextMeshProUGUI UpToValue;
    public GameObject RevengeObject;
    public AudioSource ClickSound;
    public TextMeshProUGUI FriendName;
    public Image FriendImage;
    public Image FriendTrophy;
    public TextMeshProUGUI FriendUpToValue;
    public List<Button> RefreshButtons;

    private PlayersTabsManager.TabsNames _currentTab;
    private List<PlayerData> _playersOpponents = new List<PlayerData>();
    private List<PlayerData> _botsOpponents = new List<PlayerData>();
    private bool _isGotPlayers;
    private bool _isGotBots;
    private bool _isRandomPlayer;
    private PlayerData _player;
    private PlayerData _revenge;
    private PlayerData _friend;
    private Sprite[] _playersAvatars;
    private Dictionary<string, ImageData> _imagesData = new Dictionary<string, ImageData>();

    private void Awake()
    {
        FuncitionsManager.Functions.GotPlayersOpponents.AddListener(OnGotPlayers);
        FuncitionsManager.Functions.GotBotsOpponents.AddListener(OnGotBots);
        StorageManager.Storage.SpriteDownloaded.AddListener(OnSpriteDownloaded);
        _playersAvatars = Resources.LoadAll<Sprite>("Sprites/avatars");
    }

    public void OnTabClick(bool isToggle)
    {
        var activeTab = Tabs.ActiveToggles().FirstOrDefault();
        var playerTabManager = activeTab.GetComponent<PlayersTabsManager>();
        if (playerTabManager != null)
        {
            var tabName = playerTabManager.TabName;
            if (tabName != _currentTab)
            {
                _currentTab = tabName;
            }
        }
    }

    public void OnEnable()
    {
        foreach (var btn in RefreshButtons)
        {
            btn.interactable = AppManager.App.CheckEnoughAmount(DatabaseManager.UserDataKeys.Coins, 10);
        }
    }

    public void GetOpponents(int min, int max)
    {
        if (_isRandomPlayer)
        {
            var upTo = int.Parse(UpToValue.text, NumberStyles.AllowThousands);
            if (upTo == GameProgressConfig.GameProgress.GetChoosenXPData().UpTo)
            {
                SetRevenge();
                ShowScreen();
                return;
            }
        }
        FuncitionsManager.Functions.GetPlayersOpponents(min, max);
        FuncitionsManager.Functions.GetBotsOpponents();
        StartCoroutine(WaitForOpponents());
    }

    IEnumerator WaitForOpponents()
    {
        while (!(_isGotPlayers && _isGotBots))
        {
            yield return null;
        }
        SetPlayer();
        SetRevenge();
        SetFriend();
    }

    private void OnGotPlayers(Players players)
    {
        _playersOpponents = players == null ? _playersOpponents = new List<PlayerData>() : players.PlayersData;
        var currentUser = _playersOpponents.Where(o => o.Uid == AppManager.App.CurrentUser.Id).FirstOrDefault();
        if (currentUser != null)
        {
            _playersOpponents.Remove(currentUser);
        }
        _isGotPlayers = true;
    }

    private void OnGotBots(Players bots)
    {
        _botsOpponents = bots == null ? new List<PlayerData>() : bots.PlayersData;
        foreach (var bot in _botsOpponents)
        {
            bot.IsBot = true;
        }
        _isGotBots = true;
    }

    public void SetPlayer()
    {
        _player = null;
        while (_player == null)
        {
            if (Random.Range(0, 100) < BotsPercents || _playersOpponents.Count == 0)
            {
                Debug.Log(_botsOpponents.Count);
                _player = _botsOpponents[Random.Range(0, _botsOpponents.Count)];
                _player.IsBot = true;
                Debug.Log(_player);
            }
            else if (_playersOpponents.Count > 0)
            {
                _player = _playersOpponents[Random.Range(0, _playersOpponents.Count)];
            }
        }
        PlayerName.text = _player.Name;
        PlayerTrophy.sprite = GetPlayerTrophy(_player);
        UpToValue.text = string.Format("{0:n0}", GameProgressConfig.GameProgress.GetChoosenXPData().UpTo);
        if (_player.AvatarIndex == -1)
        {
            if (!_imagesData.ContainsKey(_player.Uid))
            {
                _imagesData[_player.Uid] = new ImageData
                {
                    Data = _player,
                    ImageSprite = PlayerImage
                };
            }
            else
            {
                _imagesData[_player.Uid].ImageSprite = PlayerImage;
            }
            StorageManager.Storage.DownloadPlayerImage(_player.Uid, _player.ImageUrl);
        }
        else
        {
            PlayerImage.sprite = _playersAvatars[Convert.ToInt32(_player.AvatarIndex)];
            UIReferences.References.BreakIn.OpponentImage.sprite = PlayerImage.sprite;
        }
        _isRandomPlayer = true;
        ShowScreen();
    }

    private void ShowScreen()
    {
        if (UIReferences.References.GameFllow.WaitWheel.gameObject.activeInHierarchy)
        {
            UIReferences.References.GameFllow.WaitWheel.gameObject.SetActive(false);
        }
        UIReferences.References.GameFllow.ChoosePlayerBackground.gameObject.SetActive(true);
        var tabs = GetComponentInChildren<ToggleGroup>();
        tabs.GetComponentsInChildren<Toggle>()[0].isOn = true;
    }

    public void OnClick()
    {
        PlayerData opponent = null;
        switch (_currentTab)
        {
            case PlayersTabsManager.TabsNames.RandomTab:
                opponent = _player;
                UIReferences.References.BreakIn.OpponentTrophy.sprite = PlayerTrophy.sprite;
                UIReferences.References.BreakIn.OpponentImage.sprite = PlayerImage.sprite;
                if (AppManager.App.CurrentUser.Vault.Games == 1)
                {
                    DatabaseManager.Database.UpdateUserDataPrameters(new Dictionary<string, object> { { DatabaseManager.Database.GetPathFromKey(DatabaseManager.UserDataKeys.NextCharge), (DateTime.Now.AddHours(1)).ToString(UIReferences.DATE_FORMAT) }, { DatabaseManager.Database.GetPathFromKey(DatabaseManager.UserDataKeys.Games), 0 } });
                }
                else
                {
                    DatabaseManager.Database.UpdateUserDataPrameters(new Dictionary<string, object> { { DatabaseManager.Database.GetPathFromKey(DatabaseManager.UserDataKeys.Games), AppManager.App.CurrentUser.Vault.Games - 1 } });
                }
                break;
            case PlayersTabsManager.TabsNames.RevengeTab:
                opponent = _revenge;
                break;
            case PlayersTabsManager.TabsNames.FriendsTab:
                opponent = _friend;
                UIReferences.References.BreakIn.OpponentTrophy.sprite = FriendTrophy.sprite;
                UIReferences.References.BreakIn.OpponentImage.sprite = FriendImage.sprite;
                DatabaseManager.Database.UpdateUserDataPrameters(new Dictionary<string, object> { { DatabaseManager.Database.GetPathFromKey(DatabaseManager.UserDataKeys.Games), AppManager.App.CurrentUser.Vault.Games - 1 } });
                break;
        }

        AppManager.App.OpponentPlayer = opponent;
        if (!opponent.IsBot)
        {
            FuncitionsManager.Functions.SendAttackAlarm(opponent.Uid, AppManager.App.CurrentUser.Id, AppManager.App.CurrentUser.Name);
        }
        if (opponent.AvatarIndex != -1)
        {
            UIReferences.References.BreakIn.OpponentImage.sprite = _playersAvatars[Convert.ToInt32(opponent.AvatarIndex)];
        }
        gameObject.SetActive(false);
        UIReferences.References.ActiveScreen(ScreensManager.Screens.BREAKIN, ScreensManager.Requires.NONE);
    }

    public void OnRefereshClick(bool isPay)
    {
        if (isPay)
        {
            AppManager.App.CurrentUser.Vault.Coins -= 10;
            FuncitionsManager.Functions.UpdateVault(FuncitionsManager.DataKeys.Coins, (AppManager.App.CurrentUser.Vault.Coins).ToString());
            UIReferences.References.TopBar.TopBarCanvas.GetComponent<TopbarManager>().UpdateTopBar();
            foreach (var btn in RefreshButtons)
            {
                btn.interactable = AppManager.App.CheckEnoughAmount(DatabaseManager.UserDataKeys.Coins, 10);
            }
        }
        _isRandomPlayer = false;
        SetPlayer();
    }

    public void SetRevenge()
    {
        if (AppManager.App.CurrentUser.RevengesList.Count == 0)
        {
            UIReferences.References.GameFllow.NoRevengeScreen.gameObject.SetActive(true);
            UIReferences.References.GameFllow.RevengeScreen.gameObject.SetActive(false);
            return;
        }

        UIReferences.References.GameFllow.NoRevengeScreen.gameObject.SetActive(false);
        UIReferences.References.GameFllow.RevengeScreen.gameObject.SetActive(true);

        foreach (var revenge in AppManager.App.CurrentUser.RevengesList)
        {
            if (revenge.Attacker != null)
            {
                var obj = Instantiate(RevengeObject, UIReferences.References.GameFllow.RevengeContainer.transform);
                obj.transform.Find("AttackerName").GetComponent<TextMeshProUGUI>().text = revenge.Attacker.Name;
                obj.transform.Find("StolenSum").GetComponent<TextMeshProUGUI>().text = revenge.StolenSum + "M";
                if (revenge.Attacker.AvatarIndex == -1)
                {
                    if (!_imagesData.ContainsKey(revenge.Attacker.Uid))
                    {
                        _imagesData[revenge.Attacker.Uid] = new ImageData
                        {
                            Data = revenge.Attacker,
                            ImageSprite = obj.transform.Find("Mask").GetChild(0).GetComponent<Image>()
                        };
                    }
                    else
                    {
                        _imagesData[revenge.Attacker.Uid].ImageSprite = obj.transform.Find("Mask").GetChild(0).GetComponent<Image>();
                    }
                    StorageManager.Storage.DownloadPlayerImage(revenge.Attacker.Uid, revenge.Attacker.ImageUrl);
                }
                else
                {
                    obj.transform.Find("Mask").GetChild(0).GetComponent<Image>().sprite = _playersAvatars[Convert.ToInt32(revenge.Attacker.AvatarIndex)];
                }
                //RevengeBustButton
                var bustBtn = obj.transform.Find("RevengeBustButton").GetComponent<Button>();
                bustBtn.onClick.AddListener(() => { OnRevengeBustClick(revenge, obj.transform.Find("Mask").GetChild(0).GetComponent<Image>().sprite); });
            }
        }
    }

    private void OnRevengeBustClick(Revenge revenge, Sprite playerImage)
    {
        _revenge = revenge.Attacker;
        UIReferences.References.BreakIn.OpponentImage.sprite = playerImage;
        ClickSound.Play();
        OnClick();
        UIReferences.References.BreakIn.OpponentTrophy.sprite = GetPlayerTrophy(revenge.Attacker);
        AppManager.App.RemoveRevenge(revenge);
        DatabaseManager.Database.UpdateUserDataPrameters(new Dictionary<string, object> { { DatabaseManager.Database.GetPathFromKey(DatabaseManager.UserDataKeys.Games), AppManager.App.CurrentUser.Vault.Games - 1 } });
    }

    public void SetFriend()
    {
        if (AppManager.App.CurrentUser.Friends == null || AppManager.App.CurrentUser.Friends.Count == 0)
        {
            UIReferences.References.GameFllow.FriendsScreen.gameObject.SetActive(false);
            UIReferences.References.GameFllow.NoFriendsScreen.gameObject.SetActive(true);
        }
        else
        {
            UIReferences.References.GameFllow.FriendsScreen.gameObject.SetActive(true);
            UIReferences.References.GameFllow.NoFriendsScreen.gameObject.SetActive(false);

            _friend = AppManager.App.CurrentUser.Friends[Random.Range(0, AppManager.App.CurrentUser.Friends.Count)].Friend;
            FriendName.text = _friend.Name;
            PlayerTrophy.sprite = GetPlayerTrophy(_friend);
            FriendUpToValue.text = string.Format("{0:n0}", GameProgressConfig.GameProgress.GetChoosenXPData().UpTo);
            if (_friend.AvatarIndex == -1)
            {
                if (!_imagesData.ContainsKey(_friend.Uid))
                {
                    _imagesData[_friend.Uid] = new ImageData
                    {
                        Data = _friend,
                        ImageSprite = FriendImage
                    };
                }
                else
                {
                    _imagesData[_friend.Uid].ImageSprite = FriendImage;
                }
                StorageManager.Storage.DownloadPlayerImage(_friend.Uid, _friend.ImageUrl);
            }
            else
            {
                FriendImage.sprite = _playersAvatars[Convert.ToInt32(_friend.AvatarIndex)];
                UIReferences.References.BreakIn.OpponentImage.sprite = FriendImage.sprite;
            }
        }
    }

    private void OnSpriteDownloaded(string Id, Sprite sprite)
    {
        if (_imagesData.ContainsKey(Id))
        {
            _imagesData[Id].Data.PlayerImage = sprite;
            _imagesData[Id].ImageSprite.sprite = sprite;
        }
    }

    private Sprite GetPlayerTrophy(PlayerData player)
    {
        if (player.IsBot)
        {
            var degree = GameProgressConfig.GameProgress.GetCurrentXPData().Degree;
            var level = Trophies.TrophiesSprites.GetLevelByDegree(degree);
            level += Random.Range(-1, 1);
            level = Mathf.Max(0, level);
            return Trophies.TrophiesSprites.GetTrophyByDegree(Trophies.TrophiesSprites.GetDegreeByLevel(level));
        }
        else if (!string.IsNullOrEmpty(player.Degree))
        {
            return Trophies.TrophiesSprites.GetTrophyByDegree(player.Degree);
        }
        return PlayerTrophy.sprite;
    }

    private void OnDisable()
    {
        _isGotPlayers = false;
        _isGotBots = false;
    }
}

[Serializable]
class ImageData
{
    public PlayerData Data;
    public Image ImageSprite;
}