﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitingVaultWheel : AnimationCode
{
    private bool _isSpeen;
    private RectTransform _rect;
    private float _angle = 0;
    private float _lastAngle = 0;

    private void Awake()
    {
        _rect = GetComponent<RectTransform>();
    }

    private void OnEnable()
    {
        _rect.localEulerAngles = Vector3.zero;
        _isSpeen = true;
        StartCoroutine(Speen());
    }

    IEnumerator Speen()
    {
        while (_isSpeen)
        {
            _lastAngle = _angle;
            _angle = Random.Range(-360, 360);
            yield return PlayAnimationCoroutine("Speen", SpeenVaultWheel);
        }
    }

    private void SpeenVaultWheel(GameObject objectToAnimate, float percent)
    {
        _rect.localEulerAngles = Vector3.Lerp(new Vector3(0, 0, _lastAngle), new Vector3(0, 0, _angle), percent);
    }

    private void OnDisable()
    {
        _isSpeen = false;
    }
}
