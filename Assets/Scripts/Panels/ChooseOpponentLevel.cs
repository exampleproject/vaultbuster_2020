﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChooseOpponentLevel : MonoBehaviour
{
    private List<int> _uptoLevels;
    private SkillLevelButton[] _buttons;

    private void OnEnable()
    {
        if (_uptoLevels == null)
        {
            if (GameProgressConfig.GameProgress == null || AppManager.App == null || AppManager.App.CurrentUser == null)
            {
                return;
            }

            _uptoLevels = new List<int>();
            foreach (var xp in GameProgressConfig.GameProgress.GameProgressData)
            {
                if (!_uptoLevels.Contains(xp.UpTo))
                {
                    _uptoLevels.Add(xp.UpTo);
                }
            }
        }
        if (_buttons == null)
        {
            _buttons = GetComponentsInChildren<SkillLevelButton>();
        }
        for (var i = 0; i < _buttons.Length - 1; i++)
        {
            _buttons[i].Max = i < _uptoLevels.Count ? _uptoLevels[i] : _uptoLevels[_uptoLevels.Count - 1];
            _buttons[i].Min = i == 0 ? 0 : _uptoLevels[i - 1];
        }
    }
}
