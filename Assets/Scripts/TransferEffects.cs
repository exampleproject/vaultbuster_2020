﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Playables;

public class TransferEffects : OpenDialog
{
    [Space(40)]
    public TextMeshProUGUI DragLabel;
    public TextMeshProUGUI Code;
    public bool IsShield;
    public PlayableDirector BreakTimeline;
    public PlayableDirector ShieldTimeline;
    private string _dragLabelText;
    private string _opponentValutName;
    private string _secondWinLabelText;

    private void Awake()
    {
        _dragLabelText = DragLabel.text;
        _opponentValutName = UIReferences.References.BreakIn.OpponentVault.text;
        _secondWinLabelText = UIReferences.References.BreakIn.SecondWinLabel.text;
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        //UIReferences.References.BreakIn.OpponentVault.text = string.Format(_opponentValutName, AppManager.App.OpponentPlayer.Name);
        //UIReferences.References.BreakIn.SecondWinLabel.text = string.Format(_secondWinLabelText, AppManager.App.OpponentPlayer.Name);
        string firstName = AppManager.App.OpponentPlayer.Name;
        if (AppManager.App.OpponentPlayer.Name.Contains("_"))
        {
            firstName = AppManager.App.OpponentPlayer.Name.Split('_')[0];
        }
        else if (AppManager.App.OpponentPlayer.Name.Contains(" "))
        {
            firstName = AppManager.App.OpponentPlayer.Name.Split()[0];
        }
        DragLabel.text = string.Format(_dragLabelText, firstName);

        if (AppManager.App.Reward.IsShieldBlock == "true")
        {
            BreakTimeline.Stop();
            BreakTimeline.time = 0;
            ShieldTimeline.Play();
        }
        else
        {
            if (UIReferences.References.BreakIn.ShieldsElements.gameObject.activeInHierarchy)
            {
                UIReferences.References.BreakIn.ShieldsElements.gameObject.SetActive(false);
            }
            ShieldTimeline.Stop();
            ShieldTimeline.time = 0;
            BreakTimeline.Play();
        }

        base.OnEnable();
    }

    public void Close()
    {
        UIReferences.References.ActiveScreen(ScreensManager.Screens.REWARD, ScreensManager.Requires.NONE);
    }

    private void OnDisable()
    {
        foreach (var timeline in GetComponentsInChildren<PlayableDirector>())
        {
            timeline.Stop();
        }
        for (var i = 1; i <= 5; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }

        UIReferences.References.GameFllow.ChoosePlayerScreen.GetComponent<ChoosePlayer>().OnRefereshClick(false);
    }
}
