using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using TMPro;

public class RechargeTimer : MonoBehaviour
{
    private const string TIMER_FORMAT = "COME BACK IN {0}M : {1}S MINUTES\nFOR FREE<color=#DC55F9>5 GAMES</color> PLUS <color=#DC55F9>3 BOOSTERS</color>";
    private TextMeshProUGUI _timeText;

    private void Awake()
    {
        _timeText = GetComponent<TextMeshProUGUI>();
    }

    private void OnEnable()
    {
        StartCoroutine(Timer());
    }

    IEnumerator Timer()
    {
        if (AppManager.App != null && AppManager.App.CurrentUser != null)
        {
            if (!string.IsNullOrEmpty(AppManager.App.CurrentUser.NextCharge))
            {
                DateTime nextCharge;
                if (DateTime.TryParseExact(AppManager.App.CurrentUser.NextCharge, UIReferences.DATE_FORMAT, CultureInfo.InvariantCulture, DateTimeStyles.AllowInnerWhite, out nextCharge))
                {
                    var timeGap = nextCharge - DateTime.Now;
                    while (timeGap.TotalSeconds > 1)
                    {
                        timeGap = nextCharge - DateTime.Now;
                        _timeText.text = string.Format(TIMER_FORMAT, timeGap.Minutes, timeGap.Seconds);

                        yield return new WaitForSeconds(1);
                    }
                }
            }
        }
        _timeText.text = string.Format(TIMER_FORMAT, 0, 0);
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }
}
