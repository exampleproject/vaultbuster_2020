﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShapeArrow : MonoBehaviour
{
    public CodeShapes Shape;

    public float ApearTime = 0.5f;
    public float ShowTime = 2f;
    public float FadoutTime = 0.25f;

    public Image ShapeImage;

    public Sprite Right;
    public Sprite Left;
    public Sprite Up;
    public Sprite Down;
    public Sprite RightUp;
    public Sprite RightDown;
    public Sprite LeftUp;
    public Sprite LeftDown;
    public Sprite RightUpRight;
    public Sprite RightDownRight;
    public Sprite LeftUpLeft;
    public Sprite LeftDownLeft;
    public Sprite UpRightUp;
    public Sprite UpLeftUp;
    public Sprite DownRightDown;
    public Sprite DownLeftDown;
    public Sprite ZigzagRightUpRight;
    public Sprite ZigzagRightDownRight;
    public Sprite ZigzagLeftUpLeft;
    public Sprite ZigzagLeftDownLeft;
    public Sprite ZigzagUpRightUp;
    public Sprite ZigzagUpLeftUp;
    public Sprite ZigzagDownRightDown;
    public Sprite ZigzagDownLeftDown;

    private void OnEnable()
    {
        StartCoroutine(ShapeAnimation());
    }

    IEnumerator ShapeAnimation()
    {
        ShapeImage.color = new Color(ShapeImage.color.r, ShapeImage.color.g, ShapeImage.color.b, 0);
        switch (Shape)
        {
            case CodeShapes.Right:
                ShapeImage.sprite = Right;
                break;
            case CodeShapes.Left:
                ShapeImage.sprite = Left;
                break;
            case CodeShapes.Up:
                ShapeImage.sprite = Up;
                break;
            case CodeShapes.Down:
                ShapeImage.sprite = Down;
                break;
            case CodeShapes.RightUp:
                ShapeImage.sprite = RightUp;
                break;
            case CodeShapes.RightDown:
                ShapeImage.sprite = RightDown;
                break;
            case CodeShapes.LeftUp:
                ShapeImage.sprite = LeftUp;
                break;
            case CodeShapes.LeftDown:
                ShapeImage.sprite = LeftDown;
                break;
            case CodeShapes.RightUpRight:
                ShapeImage.sprite = RightUpRight;
                break;
            case CodeShapes.RightDownRight:
                ShapeImage.sprite = RightDownRight;
                break;
            case CodeShapes.LeftUpLeft:
                ShapeImage.sprite = LeftUpLeft;
                break;
            case CodeShapes.LeftDownLeft:
                ShapeImage.sprite = LeftDownLeft;
                break;
            case CodeShapes.UpRightUp:
                ShapeImage.sprite = UpRightUp;
                break;
            case CodeShapes.UpLeftUp:
                ShapeImage.sprite = UpLeftUp;
                break;
            case CodeShapes.DownRightDown:
                ShapeImage.sprite = DownRightDown;
                break;
            case CodeShapes.DownLeftDown:
                ShapeImage.sprite = DownLeftDown;
                break;
            case CodeShapes.ZigzagRightUp:
                ShapeImage.sprite = ZigzagRightUpRight;
                break;
            case CodeShapes.ZigzagRightDown:
                ShapeImage.sprite = ZigzagRightDownRight;
                break;
            case CodeShapes.ZigzagLeftUp:
                ShapeImage.sprite = ZigzagLeftUpLeft;
                break;
            case CodeShapes.ZigzagLeftDown:
                ShapeImage.sprite = ZigzagLeftDownLeft;
                break;
            case CodeShapes.ZigzagUpRight:
                ShapeImage.sprite = ZigzagUpRightUp;
                break;
            case CodeShapes.ZigzagUpLeft:
                ShapeImage.sprite = ZigzagUpLeftUp;
                break;
            case CodeShapes.ZigzagDownRight:
                ShapeImage.sprite = ZigzagDownRightDown;
                break;
            case CodeShapes.ZigzagDownLeft:
                ShapeImage.sprite = ZigzagDownLeftDown;
                break;
        }

        for (float timer = 0; timer <= ApearTime; timer += Time.deltaTime)
        {
            ShapeImage.color = new Color(ShapeImage.color.r, ShapeImage.color.g, ShapeImage.color.b, timer / ApearTime);
            yield return null;
        }
        ShapeImage.color = new Color(ShapeImage.color.r, ShapeImage.color.g, ShapeImage.color.b, 1);

        yield return new WaitForSecondsRealtime(ShowTime);

        for (float timer = FadoutTime; timer > 0; timer -= Time.deltaTime)
        {
            ShapeImage.color = new Color(ShapeImage.color.r, ShapeImage.color.g, ShapeImage.color.b, timer / FadoutTime);
        }
        ShapeImage.color = new Color(ShapeImage.color.r, ShapeImage.color.g, ShapeImage.color.b, 0);

        gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        StopAllCoroutines();
        ShapeImage.color = new Color(ShapeImage.color.r, ShapeImage.color.g, ShapeImage.color.b, 0);
        gameObject.SetActive(false);
    }
}
