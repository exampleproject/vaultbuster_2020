﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopButton : MonoBehaviour
{
    public AudioSource ClickSound { get; set; }
    public int Sum { get; set; }
    public int Price { get; set; }

    private Dictionary<string, object> _updateValues = new Dictionary<string, object>();
    private string _keyPath;
    private string _goldKeyPath;

    public DatabaseManager.UserDataKeys Key
    {
        get
        {
            return _key;
        }
        set
        {
            _key = value;
            _keyPath = DatabaseManager.Database.GetPathFromKey(_key);
            _goldKeyPath = DatabaseManager.Database.GetPathFromKey(DatabaseManager.UserDataKeys.GoldBars);
            if (_updateValues.ContainsKey(_keyPath))
            {
                _updateValues[_keyPath] = (int)DatabaseManager.Database.GetObjectByKey(_key);
            }
            else
            {
                _updateValues.Add(_keyPath, (int)DatabaseManager.Database.GetObjectByKey(_key));
            }
            if (_updateValues.ContainsKey(_goldKeyPath))
            {
                _updateValues[_goldKeyPath] = AppManager.App.CurrentUser.Vault.GoldBars;
            }
            else
            {
                _updateValues.Add(_goldKeyPath, AppManager.App.CurrentUser.Vault.GoldBars);
            }
        }
    }
    private DatabaseManager.UserDataKeys _key;

    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(OnBuyClick);
    }

    private void OnBuyClick()
    {
        if (AppManager.App.CurrentUser.Vault.GoldBars >= Price)
        {
            int newSum = 0;
            switch (_key)
            {
                case DatabaseManager.UserDataKeys.Coins:
                    newSum = AppManager.App.CurrentUser.Vault.Coins + Sum;
                    break;
                case DatabaseManager.UserDataKeys.Boosters:
                    newSum = AppManager.App.CurrentUser.Vault.Boosters + Sum;
                    break;
                case DatabaseManager.UserDataKeys.Games:
                    newSum = AppManager.App.CurrentUser.Vault.Games + Sum;
                    break;
                case DatabaseManager.UserDataKeys.Shields:
                    newSum = AppManager.App.CurrentUser.Vault.Shields + Sum;
                    break;
            }
            ClickSound.Play();
            _updateValues[_keyPath] = newSum;
            _updateValues[_goldKeyPath] = AppManager.App.CurrentUser.Vault.GoldBars - Price;
            DatabaseManager.Database.UpdateUserDataPrameters(_updateValues);
        }
        else
        {
            UIReferences.References.ShowMessage(UIReferences.MessagesTypes.NotEnoughGlodBarsMessage);
        }
    }
}
